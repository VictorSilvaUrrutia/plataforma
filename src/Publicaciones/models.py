from django.db import models


class Avpublicaciongeneral(models.Model):
    cod_publicacion = models.IntegerField(primary_key=True)
    nombre = models.CharField(max_length=600, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'avpublicaciongeneral'

    def __str__(self):
        return self.nombre