import sys
from itertools import cycle

from django.db import models
from datetime import datetime


class Cuentas(models.Model):

    def save(self):
        "Toma el valor maximo de la columna de la base de datos y lo autoincrementa antes de guardar"
        top = Cuentas.objects.order_by('-id_cuenta', )[0]
        self.id_cuenta = top.id_cuenta + 1
        super(Cuentas, self).save()

    id_cuenta = models.IntegerField(primary_key=True,verbose_name='Código',editable=False)
    descripcion = models.CharField(max_length=138, blank=True, null=True,verbose_name='Descripción',unique=True)
    grupo = models.IntegerField(max_length=6, blank=True, null=True,verbose_name='Grupo')
    clase = models.IntegerField(max_length=9, blank=True, null=True,verbose_name='Clase')
    codigo = models.IntegerField(max_length=9, blank=True, null=True,verbose_name='Código')
    digito = models.IntegerField(max_length=3, blank=True, null=True,verbose_name='Dígito')
    item = models.IntegerField(max_length=6, blank=True, null=True,verbose_name='Ítem')

    class Meta:
        verbose_name = 'Cuenta'
        verbose_name_plural = 'Cuentas'
        managed = False
        db_table = 'cuentas'

    def __str__(self):
        return '{0} ({1})'.format(self.id_cuenta, self.descripcion)




class Avagentes(models.Model):

    def save(self):
        "Toma el valor maximo de la columna de la base de datos y lo autoincrementa antes de guardar"
        top = Avagentes.objects.order_by('-codigo',)[0]
        self.codigo = top.codigo + 1
        super(Avagentes, self).save()

    Inactivo = 0
    Activo = 1

    SELECCION_ESTADO = (
        (Inactivo, 0),
        (Activo, 1)
    )

    codigo = models.CharField(primary_key=True,editable=False,db_column='CODIGO',max_length=30)
    agente = models.CharField(max_length=99, blank=True, null=True,verbose_name="Agente",help_text="Escriba el Nombre del Agente",unique=True)
    comision = models.IntegerField(blank=True, null=True,verbose_name="Comisión")
    codciudad = models.CharField(max_length=999, null=True,verbose_name='Ingrese codigo postal')
    estado = models.IntegerField(choices=SELECCION_ESTADO, verbose_name='activo',
                                    help_text='Seleccione "0" para INACTIVO y "1" para ACTIVO', )

    class Meta:
        verbose_name = 'Agente'
        verbose_name_plural = 'Agentes'
        managed = False
        db_table = 'avagentes'

    def __str__(self):
        return '{0} ({1})'.format(self.codigo, self.agente)



class Clientes(models.Model):




    Inactivo=0
    Activo=1

    SELECCION_ESTADO = (
        (Inactivo,0),
        (Activo,1)
    )

    rut = models.CharField(primary_key=True,help_text='Ingrese Rut sin puntos,guión y sin digito verificador',unique=True,max_length=99)
    digito = models.CharField( blank=True, null=False,max_length=1)
    nombre = models.CharField(max_length=999, blank=True, null=True,verbose_name='Nombre/Razón Social')
    giro = models.CharField(max_length=450, blank=True, null=True, help_text='Ingrese actividad o negocio')
    direccion = models.CharField(max_length=600, blank=True, null=True)
    ciudad = models.CharField(max_length=150, blank=True, null=True)
    comuna = models.CharField(max_length=150, blank=True, null=True)
    telefono = models.CharField(max_length=75, blank=True, null=True)
    celular = models.CharField(max_length=60, blank=True, null=True, default='+569')
    correo = models.EmailField(max_length=150, blank=True, null=True)
    full_bloqueo = models.BigIntegerField(blank=True, null=True,default=0,editable=False)
    semi_bloqueo = models.BigIntegerField(blank=True, null=True,default=0,editable=False)
    autorizado = models.BigIntegerField(blank=True, null=True,default=1,editable=False)
    relacionado = models.BigIntegerField(default=0,editable=False)
    id_cuenta = models.ForeignKey(Cuentas,  db_column='id_cuenta', blank=True, null=True,verbose_name='Cuenta contable',on_delete=models.PROTECT)
    nro_cliente = models.BigIntegerField(blank=True, null=True,default=1,editable=False)
    estado = models.BigIntegerField(choices=SELECCION_ESTADO,blank=True, null=True,verbose_name='activo',help_text='Seleccione "0" para INACTIVO y "1" para ACTIVO',default=1)
    fecha_modificacion = models.DateField(("Fecha de Ingreso"), default=datetime.now(),editable=False)
    agente = models.ForeignKey(Avagentes,  db_column='AGENTE', blank=True, null=True,verbose_name='Agente asignado',default=51,on_delete=models.PROTECT)

    class Meta:
        verbose_name = 'Clientes'
        verbose_name_plural = 'Clientes'
        managed = False
        db_table = 'clientes'

    def __str__(self):
        return '{0} ({1})'.format(self.rut, self.nombre)

class Avtarifa(models.Model):

    def save(self):
        "Toma el valor maximo de la columna de la base de datos y lo autoincrementa antes de guardar"
        top = Avtarifa.objects.order_by('-codigo', )[0]
        self.codigo = top.codigo + 1
        super(Avtarifa, self).save()

    codigo = models.IntegerField(primary_key=True, editable=False)
    descripcion = models.CharField(max_length=450, blank=True, null=True, verbose_name='Descripción', unique=True)
    valorminimo = models.IntegerField(max_length=21, blank=True, null=True, verbose_name='Valor mínimo')
    valorpalabra = models.IntegerField(max_length=21, blank=True, null=True, verbose_name='Valor palabra')
    valorcentim = models.IntegerField(max_length=12, blank=True, null=True, verbose_name='Valor centimetro')
    valorcentimsemana = models.IntegerField(max_length=60, blank=True, null=True, verbose_name='Día semana')
    valorcentimdomingo = models.IntegerField(max_length=60, blank=True, null=True, verbose_name='Día domingo')
    valorcentimsabado = models.IntegerField(blank=True, null=True, verbose_name='Día sabado', editable=False,
                                                default=300)
    valorlogo = models.IntegerField(max_length=12, blank=True, null=True, verbose_name='Valor logo')

    cod_unid_negocio = models.CharField(max_length=60, blank=True, null=True, editable=False)
    id_cuenta = models.ForeignKey(Cuentas, verbose_name='Cuenta contable', db_column='ID_CUENTA', null=True,
                                      blank=True,on_delete=models.PROTECT)

    class Meta:
        verbose_name = 'Avisos/Tarifa'
        verbose_name_plural = 'Tarifas'
        managed = False
        db_table = 'avtarifa'

    def __str__(self):
        return '{0} ({1})'.format(self.codigo, self.descripcion)



class Documentos(models.Model):

    def save(self):
        "Toma el valor maximo de la columna de la base de datos y lo autoincrementa antes de guardar"
        top = Documentos.objects.order_by('-codigo')[0]
        self.codigo = top.codigo + 1
        super(Documentos, self).save()


    codigo = models.IntegerField(primary_key=True,editable=False)
    descripcion = models.CharField(max_length=600,help_text='Escriba una descripción para documentos a emitir')

    class Meta:
        managed = False
        db_table = 'documentos'
        verbose_name = 'Documento'
        verbose_name_plural = 'Documentos'
        ordering = ["codigo"]

    def __str__(self):
        return '{0} ({1})'.format(self.codigo, self.descripcion)










