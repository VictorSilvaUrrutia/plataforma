from django.contrib import admin
from .models import Avagentes
from .models import Clientes
from .models import Avtarifa
from .models import Cuentas
from .models import Documentos



#Lo que se va a visualizar en el panel de administración
#Crear una clase Admin de cada modelo como se ve abajo
class AdminDocumentos(admin.ModelAdmin):
    list_display = ["codigo", "descripcion",]



class AdminClientes(admin.ModelAdmin):
    list_display = ["nombre",  "direccion", "ciudad" ,'giro','telefono','fecha_modificacion','correo',]
    fields = [('rut','digito'),'nombre','giro','direccion',('ciudad','comuna'),('telefono','celular'),'correo','id_cuenta','estado','agente']
    ordering = ('fecha_modificacion',)
    search_fields = ["nombre",  "correo","rut"]
    list_display_links = ["nombre","correo"]


class AdminAvagentes(admin.ModelAdmin):

    list_display = ["codigo","agente","estado"]
    ordering = ('codigo',)
    list_display_links = ["agente"]


#Esta función permite bloquear que cualquier usuario elimine un registro especifico de cada modelo

    #def get_actions(self, request):
        #actions = super().get_actions(request)
        #if 'delete_selected' in actions:
            #del actions['delete_selected']
        #return actions

class AdminAvtarifa(admin.ModelAdmin):
    list_display = ["codigo", "descripcion", "valorminimo", "valorpalabra", "valorcentim", "valorlogo",
                        "valorcentimdomingo", "valorcentimsemana", "valorcentimsabado"]
    fields = ['descripcion', ('valorminimo', 'valorpalabra'), ('valorcentim', 'valorcentimsemana'),
                  ('valorcentimdomingo', 'valorlogo'), 'id_cuenta']

    ordering = ('codigo',)




class AdminCuentas(admin.ModelAdmin):
    list_display = ["id_cuenta",  "descripcion", "grupo" ,"clase","codigo","digito","item"]
    fields = ['descripcion',('grupo', 'clase','codigo','digito','item')]

    ordering = ('id_cuenta',)



#Registrar modelo y el 'Admin'de cada modelo como se muestra a continuación
admin.site.register(Documentos, AdminDocumentos,)
admin.site.register(Avtarifa, AdminAvtarifa)
admin.site.register(Avagentes,AdminAvagentes)
admin.site.register(Clientes, AdminClientes)
admin.site.register(Cuentas, AdminCuentas)







admin.site.site_header = "Bienvenido a página de administración PLATAFORMA"







