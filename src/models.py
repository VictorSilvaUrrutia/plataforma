# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey has `on_delete` set to the desired behavior.
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from __future__ import unicode_literals

from django.db import models


class Actividad(models.Model):
    rut = models.FloatField(blank=True, null=True)
    indice_act = models.FloatField(blank=True, null=True)
    nombre = models.CharField(max_length=450, blank=True, null=True)
    direccion = models.CharField(max_length=450, blank=True, null=True)
    ciudad = models.CharField(max_length=105, blank=True, null=True)
    comuna = models.CharField(max_length=60, blank=True, null=True)
    giro = models.CharField(max_length=450, blank=True, null=True)
    telefono = models.CharField(max_length=36, blank=True, null=True)
    relacionado = models.CharField(max_length=3, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'actividad'


class AdjuntosOp(models.Model):
    id_op = models.BigIntegerField(blank=True, null=True)
    orden_agencia = models.CharField(max_length=600, blank=True, null=True)
    orden_compra = models.CharField(max_length=600, blank=True, null=True)
    orden_email = models.CharField(max_length=600, blank=True, null=True)
    orden_diario = models.CharField(max_length=600, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'adjuntos_op'


class Agencias(models.Model):
    nombre = models.CharField(primary_key=True, max_length=450)
    codigo = models.BigIntegerField(blank=True, null=True)
    nombre_contacto = models.CharField(max_length=450, blank=True, null=True)
    cargo_contacto = models.CharField(max_length=450, blank=True, null=True)
    fono_contacto = models.BigIntegerField(blank=True, null=True)
    mail_contacto = models.CharField(max_length=450, blank=True, null=True)
    ciudad = models.CharField(max_length=90, blank=True, null=True)
    estado = models.BigIntegerField(blank=True, null=True)
    porcentaje = models.BigIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'agencias'


class AgenciasOp(models.Model):
    nombre = models.CharField(max_length=450, blank=True, null=True)
    ciudad = models.CharField(max_length=450, blank=True, null=True)
    nombre_contacto = models.CharField(max_length=450, blank=True, null=True)
    cargo_contacto = models.CharField(max_length=450, blank=True, null=True)
    fono_contacto = models.BigIntegerField(blank=True, null=True)
    mail_contacto = models.CharField(max_length=450, blank=True, null=True)
    numero_op = models.BigIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'agencias_op'


class AuthGroup(models.Model):
    id = models.IntegerField(primary_key=True)  # AutoField?
    name = models.TextField(unique=True, blank=True, null=True)  # This field type is a guess.

    class Meta:
        managed = False
        db_table = 'auth_group'


class AuthGroupPermissions(models.Model):
    id = models.IntegerField(primary_key=True)  # AutoField?
    group = models.ForeignKey(AuthGroup, models.DO_NOTHING)
    permission = models.ForeignKey('AuthPermission', models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_group_permissions'
        unique_together = (('group', 'permission'),)


class AuthPermission(models.Model):
    id = models.IntegerField(primary_key=True)  # AutoField?
    name = models.TextField(blank=True, null=True)  # This field type is a guess.
    content_type = models.ForeignKey('DjangoContentType', models.DO_NOTHING)
    codename = models.TextField(blank=True, null=True)  # This field type is a guess.

    class Meta:
        managed = False
        db_table = 'auth_permission'
        unique_together = (('content_type', 'codename'),)


class AuthUser(models.Model):
    id = models.IntegerField(primary_key=True)  # AutoField?
    password = models.TextField(blank=True, null=True)  # This field type is a guess.
    last_login = models.DateTimeField(blank=True, null=True)
    is_superuser = models.BooleanField()
    username = models.TextField(unique=True, blank=True, null=True)  # This field type is a guess.
    first_name = models.TextField(blank=True, null=True)  # This field type is a guess.
    last_name = models.TextField(blank=True, null=True)  # This field type is a guess.
    email = models.TextField(blank=True, null=True)  # This field type is a guess.
    is_staff = models.BooleanField()
    is_active = models.BooleanField()
    date_joined = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'auth_user'


class AuthUserGroups(models.Model):
    id = models.IntegerField(primary_key=True)  # AutoField?
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)
    group = models.ForeignKey(AuthGroup, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_user_groups'
        unique_together = (('user', 'group'),)


class AuthUserUserPermissions(models.Model):
    id = models.IntegerField(primary_key=True)  # AutoField?
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)
    permission = models.ForeignKey(AuthPermission, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_user_user_permissions'
        unique_together = (('user', 'permission'),)


class Avagentes(models.Model):
    codigo = models.BigIntegerField(blank=True, null=True)
    agente = models.CharField(max_length=165, blank=True, null=True)
    comision = models.BigIntegerField(blank=True, null=True)
    estado = models.BigIntegerField(blank=True, null=True)
    codciudad = models.CharField(max_length=450, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'avagentes'


class Avfolios(models.Model):
    folio = models.CharField(max_length=15, blank=True, null=True)
    fecha = models.CharField(max_length=36, blank=True, null=True)
    observaciones = models.CharField(max_length=1050, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'avfolios'


class Avisos(models.Model):
    numero = models.CharField(primary_key=True, max_length=18)
    rut_cliente = models.CharField(max_length=30, blank=True, null=True)
    id_agente = models.CharField(max_length=6, blank=True, null=True)
    id_tipo = models.CharField(max_length=3, blank=True, null=True)
    id_seccion = models.CharField(max_length=12, blank=True, null=True)
    id_tarifa = models.CharField(max_length=9, blank=True, null=True)
    estado = models.CharField(max_length=3, blank=True, null=True)
    glosa_simple = models.CharField(max_length=9000, blank=True, null=True)
    glosa_aviso = models.CharField(max_length=12000, blank=True, null=True)
    total_pagar = models.CharField(max_length=45, blank=True, null=True)
    fecha_input = models.CharField(max_length=36, blank=True, null=True)
    id_actividad = models.CharField(max_length=3, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'Clientes'


class Avpalabras(models.Model):
    id_aviso = models.BigIntegerField(blank=True, null=True)
    id_palabra = models.IntegerField(blank=True, null=True)
    palabra = models.CharField(max_length=2700, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'avpalabras'


class Avpalabras2(models.Model):
    id_aviso = models.CharField(max_length=18, blank=True, null=True)
    id_palabra = models.CharField(max_length=45, blank=True, null=True)
    palabra = models.CharField(max_length=2700, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'avpalabras2'


class Avpalabras3(models.Model):
    id_aviso = models.CharField(max_length=18, blank=True, null=True)
    id_palabra = models.CharField(max_length=45, blank=True, null=True)
    palabra = models.CharField(max_length=2700, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'avpalabras3'


class Avpauta(models.Model):
    fecha = models.CharField(max_length=45, blank=True, null=True)
    seccion = models.CharField(max_length=150, blank=True, null=True)
    tipo_aviso = models.CharField(max_length=450, blank=True, null=True)
    glosa_aviso = models.CharField(max_length=12000, blank=True, null=True)
    documentos = models.CharField(max_length=75, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'avpauta'


class Avpautanotas(models.Model):
    fecha = models.DateField(blank=True, null=True)
    cod_notaventa = models.IntegerField(blank=True, null=True)
    numero = models.BigIntegerField(blank=True, null=True)
    estado_publicacion = models.BigIntegerField(blank=True, null=True)
    cod_ot = models.BigIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'avpautanotas'


class Avpublicaciones(models.Model):
    id_aviso = models.CharField(max_length=18, blank=True, null=True)
    fecha = models.CharField(max_length=45, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'avpublicaciones'


class Avpublicaciongeneral(models.Model):
    cod_publicacion = models.IntegerField()
    nombre = models.CharField(max_length=600, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'avpublicaciongeneral'


class Avseccion(models.Model):
    codigo = models.BigIntegerField(blank=True, null=True)
    seccion = models.CharField(max_length=450, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'avseccion'


class Avsecciondiario(models.Model):
    cod_seccion = models.BigIntegerField(primary_key=True)
    nombre = models.CharField(max_length=600, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'avsecciondiario'


class Avtarifa(models.Model):
    codigo = models.BigIntegerField(blank=True, null=True)
    descripcion = models.CharField(max_length=450, blank=True, null=True)
    valorminimo = models.CharField(max_length=21, blank=True, null=True)
    valorpalabra = models.CharField(max_length=21, blank=True, null=True)
    valorcentim = models.CharField(max_length=12, blank=True, null=True)
    valorlogo = models.CharField(max_length=12, blank=True, null=True)
    id_cuenta = models.CharField(max_length=6, blank=True, null=True)
    cod_unid_negocio = models.CharField(max_length=60, blank=True, null=True)
    valorcentimdomingo = models.CharField(max_length=60, blank=True, null=True)
    valorcentimsemana = models.CharField(max_length=60, blank=True, null=True)
    valorcentimsabado = models.BigIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'avtarifa'


class Avtipo(models.Model):
    codigo = models.CharField(max_length=3, blank=True, null=True)
    tipo = models.CharField(max_length=90, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'avtipo'


class Bancos(models.Model):
    id_banco = models.CharField(max_length=6, blank=True, null=True)
    nombre = models.CharField(max_length=54, blank=True, null=True)
    plaza = models.CharField(max_length=60, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'bancos'


class Boletas(models.Model):
    numero = models.BigIntegerField(blank=True, null=True)
    fecha_emision = models.DateField(blank=True, null=True)
    rut_cliente = models.CharField(max_length=30, blank=True, null=True)
    nombre = models.CharField(max_length=180, blank=True, null=True)
    pago_efectivo = models.CharField(max_length=30, blank=True, null=True)
    documentos = models.CharField(max_length=3, blank=True, null=True)
    total_pagar = models.CharField(max_length=36, blank=True, null=True)
    estado = models.BigIntegerField(blank=True, null=True)
    notasventa = models.CharField(max_length=3, blank=True, null=True)
    descuento = models.CharField(max_length=3, blank=True, null=True)
    id_user = models.CharField(max_length=6, blank=True, null=True)
    is_reparto = models.CharField(max_length=3, blank=True, null=True)
    formapago = models.CharField(max_length=60, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'boletas'


class BoletasAgente(models.Model):
    numero = models.BigIntegerField(blank=True, null=True)
    id_cliente = models.BigIntegerField(blank=True, null=True)
    fecha_emision = models.DateField(blank=True, null=True)
    valor_normal = models.BigIntegerField(blank=True, null=True)
    valor_domingo = models.BigIntegerField(blank=True, null=True)
    ejemplares_semana = models.BigIntegerField(blank=True, null=True)
    ejemplares_domingo = models.BigIntegerField(blank=True, null=True)
    iva = models.BigIntegerField(blank=True, null=True)
    total_boleta = models.BigIntegerField(blank=True, null=True)
    descuento = models.BigIntegerField(blank=True, null=True)
    glosa_remesa = models.CharField(max_length=900, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'boletas_agente'


class BoletasDocumentos(models.Model):
    id_boleta = models.BigIntegerField(blank=True, null=True)
    id_tipo_docto = models.BigIntegerField(blank=True, null=True)
    serie = models.CharField(max_length=150, blank=True, null=True)
    nro_documento = models.BigIntegerField(blank=True, null=True)
    fecha_input = models.DateField(blank=True, null=True)
    monto = models.BigIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'boletas_documentos'


class Boletasvarchar(models.Model):
    numero = models.BigIntegerField(blank=True, null=True)
    fecha_emision = models.DateField(blank=True, null=True)
    rut_cliente = models.CharField(max_length=30, blank=True, null=True)
    nombre = models.CharField(max_length=180, blank=True, null=True)
    pago_efectivo = models.CharField(max_length=30, blank=True, null=True)
    documentos = models.CharField(max_length=3, blank=True, null=True)
    total_pagar = models.CharField(max_length=36, blank=True, null=True)
    estado = models.CharField(max_length=3, blank=True, null=True)
    notasventa = models.CharField(max_length=3, blank=True, null=True)
    descuento = models.CharField(max_length=3, blank=True, null=True)
    id_user = models.CharField(max_length=3, blank=True, null=True)
    is_reparto = models.CharField(max_length=3, blank=True, null=True)
    formapago = models.CharField(max_length=60, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'boletasvarchar'


class CartolaFacturas(models.Model):
    id_cartola = models.FloatField(blank=True, null=True)
    id_factura = models.FloatField(blank=True, null=True)
    estado = models.BigIntegerField(blank=True, null=True)
    monto = models.FloatField(blank=True, null=True)
    pendiente = models.CharField(max_length=33, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'cartola_facturas'


class CartolasClientes(models.Model):
    id_cartola = models.FloatField(blank=True, null=True)
    rut = models.CharField(max_length=36, blank=True, null=True)
    id_actividad = models.FloatField(blank=True, null=True)
    saldo = models.FloatField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'cartolas_clientes'


class Clientes(models.Model):
    rut = models.FloatField(blank=True, null=True)
    digito = models.CharField(max_length=3, blank=True, null=True)
    nombre = models.CharField(max_length=450, blank=True, null=True)
    direccion = models.CharField(max_length=600, blank=True, null=True)
    ciudad = models.CharField(max_length=150, blank=True, null=True)
    comuna = models.CharField(max_length=150, blank=True, null=True)
    giro = models.CharField(max_length=450, blank=True, null=True)
    telefono = models.CharField(max_length=75, blank=True, null=True)
    full_bloqueo = models.BigIntegerField(blank=True, null=True)
    semi_bloqueo = models.BigIntegerField(blank=True, null=True)
    autorizado = models.BigIntegerField(blank=True, null=True)
    relacionado = models.BigIntegerField(blank=True, null=True)
    id_cuenta = models.BigIntegerField(blank=True, null=True)
    nro_cliente = models.BigIntegerField(blank=True, null=True)
    estado = models.BigIntegerField(blank=True, null=True)
    fecha_modificacion = models.DateField(blank=True, null=True)
    celular = models.CharField(max_length=60, blank=True, null=True)
    correo = models.CharField(max_length=150, blank=True, null=True)
    agente = models.CharField(max_length=60, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'clientes'


class ComprobantePago(models.Model):
    id_comprobante = models.IntegerField(blank=True, null=True)
    nombre = models.CharField(max_length=450, blank=True, null=True)
    rut = models.CharField(max_length=36, blank=True, null=True)
    nro_documento = models.IntegerField(blank=True, null=True)
    tipo_documento = models.CharField(max_length=3, blank=True, null=True)
    fecha_emision = models.CharField(max_length=36, blank=True, null=True)
    fecha_vence = models.CharField(max_length=36, blank=True, null=True)
    valor_documento = models.CharField(max_length=21, blank=True, null=True)
    monto_pagado = models.CharField(max_length=21, blank=True, null=True)
    saldo = models.CharField(max_length=21, blank=True, null=True)
    glosa = models.CharField(max_length=450, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'comprobante_pago'


class Config(models.Model):
    iva = models.CharField(max_length=6, blank=True, null=True)
    path_notas = models.CharField(max_length=132, blank=True, null=True)
    path_clientes = models.CharField(max_length=60, blank=True, null=True)
    name_clientes = models.CharField(max_length=60, blank=True, null=True)
    print_notas = models.CharField(max_length=60, blank=True, null=True)
    print_boletas = models.CharField(max_length=90, blank=True, null=True)
    print_informes = models.CharField(max_length=24, blank=True, null=True)
    print_reportes = models.CharField(max_length=24, blank=True, null=True)
    minimo_palabras = models.CharField(max_length=6, blank=True, null=True)
    maximo_palabras = models.CharField(max_length=6, blank=True, null=True)
    print_facturas = models.CharField(max_length=60, blank=True, null=True)
    year = models.CharField(max_length=12, blank=True, null=True)
    vaucher = models.CharField(max_length=18, blank=True, null=True)
    print_comprobantes = models.CharField(max_length=60, blank=True, null=True)
    max_paginas = models.CharField(max_length=60, blank=True, null=True)
    clave = models.CharField(max_length=60, blank=True, null=True)
    usuario = models.CharField(max_length=60, blank=True, null=True)
    ftp = models.CharField(max_length=900, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'config'


class Cont120(models.Model):
    codigo = models.FloatField(blank=True, null=True)
    codtipocomp = models.FloatField(blank=True, null=True)
    fecha = models.DateField(blank=True, null=True)
    valor_tipocambio = models.FloatField(blank=True, null=True)
    marca = models.FloatField(blank=True, null=True)
    estatus = models.FloatField(blank=True, null=True)
    adicional = models.FloatField(blank=True, null=True)
    glosa = models.CharField(max_length=450, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'cont120'


class Cont121(models.Model):
    codcomprobante = models.FloatField(blank=True, null=True)
    codtipocomp = models.FloatField(blank=True, null=True)
    correlativo = models.FloatField(blank=True, null=True)
    codgrupo = models.FloatField(blank=True, null=True)
    codclase = models.FloatField(blank=True, null=True)
    codigo = models.FloatField(blank=True, null=True)
    digito = models.CharField(max_length=60, blank=True, null=True)
    coditem = models.FloatField(blank=True, null=True)
    debe_nacional = models.FloatField(blank=True, null=True)
    haber_nacional = models.FloatField(blank=True, null=True)
    debe_extrangero = models.FloatField(blank=True, null=True)
    haber_extrangero = models.FloatField(blank=True, null=True)
    glosa = models.CharField(max_length=450, blank=True, null=True)
    mes_interno = models.FloatField(blank=True, null=True)
    nro_interno = models.FloatField(blank=True, null=True)
    rutauxiliar = models.CharField(max_length=60, blank=True, null=True)
    codigoccosto = models.FloatField(blank=True, null=True)
    codtipodocto = models.FloatField(blank=True, null=True)
    serie = models.CharField(max_length=60, blank=True, null=True)
    numerodocto = models.FloatField(blank=True, null=True)
    femisiondocto = models.DateField(blank=True, null=True)
    fvencimientodocto = models.DateField(blank=True, null=True)
    numerorecepcion = models.FloatField(blank=True, null=True)
    numeroorden = models.FloatField(blank=True, null=True)
    nrodocto2 = models.FloatField(blank=True, null=True)
    grupoefe = models.FloatField(blank=True, null=True)
    claseefe = models.FloatField(blank=True, null=True)
    adicional = models.FloatField(blank=True, null=True)
    unidadcosto = models.FloatField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'cont121'


class Cont142(models.Model):
    tipo_documento = models.FloatField()
    numero_dcto = models.FloatField(blank=True, null=True)
    numero_hasta = models.FloatField(blank=True, null=True)
    local = models.FloatField(blank=True, null=True)
    fecha_emision = models.DateField(blank=True, null=True)
    fecha_vencimiento = models.DateField(blank=True, null=True)
    rut_cliente = models.CharField(max_length=60, blank=True, null=True)
    afecto = models.FloatField(blank=True, null=True)
    exento = models.FloatField(blank=True, null=True)
    iva = models.FloatField(blank=True, null=True)
    credito_especial = models.FloatField(blank=True, null=True)
    total_impuestos = models.FloatField(blank=True, null=True)
    total = models.FloatField(blank=True, null=True)
    iva_retenido = models.FloatField(blank=True, null=True)
    clietne_nro = models.FloatField(blank=True, null=True)
    marca_centralizacion = models.FloatField(blank=True, null=True)
    condiciondcto = models.FloatField(blank=True, null=True)
    tipomoneda = models.FloatField(blank=True, null=True)
    valor_tipocambio = models.FloatField(blank=True, null=True)
    afecto_us = models.FloatField(blank=True, null=True)
    exento_us = models.FloatField(blank=True, null=True)
    iva_us = models.FloatField(blank=True, null=True)
    credito_especial_us = models.FloatField(blank=True, null=True)
    total_impuestos_us = models.FloatField(blank=True, null=True)
    total_us = models.FloatField(blank=True, null=True)
    iva_retenido_us = models.FloatField(blank=True, null=True)
    marca = models.CharField(max_length=150, blank=True, null=True)
    tipocomprobcen = models.FloatField(blank=True, null=True)
    nrocomprobcen = models.FloatField(blank=True, null=True)
    fechacomprocen = models.DateField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'cont142'


class Cont143(models.Model):
    tipo_documento_l = models.FloatField()
    nro_documento_l = models.FloatField()
    correlativo = models.FloatField()
    codgrupo = models.FloatField(blank=True, null=True)
    codclase = models.FloatField(blank=True, null=True)
    codigo = models.FloatField(blank=True, null=True)
    digito = models.FloatField(blank=True, null=True)
    coditem = models.FloatField(blank=True, null=True)
    centro_resultado = models.FloatField(blank=True, null=True)
    cta_auxiliar = models.CharField(max_length=60, blank=True, null=True)
    nro_dcto = models.FloatField(blank=True, null=True)
    valor = models.FloatField(blank=True, null=True)
    valor_us = models.FloatField(blank=True, null=True)
    glosa = models.CharField(max_length=150, blank=True, null=True)
    marca_centralizacion = models.FloatField(blank=True, null=True)
    unidadcosto = models.FloatField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'cont143'


class Cont144(models.Model):
    column1 = models.CharField(max_length=180, blank=True, null=True)
    column2 = models.CharField(max_length=180, blank=True, null=True)
    column3 = models.CharField(max_length=180, blank=True, null=True)
    column4 = models.CharField(max_length=180, blank=True, null=True)
    column5 = models.CharField(max_length=180, blank=True, null=True)
    column6 = models.CharField(max_length=180, blank=True, null=True)
    column7 = models.CharField(max_length=180, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'cont144'


class Corredores(models.Model):
    codigo = models.BigIntegerField()
    seccion = models.CharField(max_length=450, blank=True, null=True)
    rut = models.CharField(max_length=60, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'corredores'


class Cuentas(models.Model):
    id_cuenta = models.BigIntegerField(blank=True, null=True)
    descripcion = models.CharField(max_length=138, blank=True, null=True)
    grupo = models.CharField(max_length=6, blank=True, null=True)
    clase = models.CharField(max_length=9, blank=True, null=True)
    codigo = models.CharField(max_length=9, blank=True, null=True)
    digito = models.CharField(max_length=3, blank=True, null=True)
    item = models.CharField(max_length=6, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'cuentas'


class Descuentos(models.Model):
    nivel = models.CharField(max_length=3, blank=True, null=True)
    desde = models.CharField(max_length=15, blank=True, null=True)
    hasta = models.CharField(max_length=48, blank=True, null=True)
    porcentaje = models.CharField(max_length=6, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'descuentos'


class DjangoAdminLog(models.Model):
    id = models.IntegerField(primary_key=True)  # AutoField?
    action_time = models.DateTimeField()
    object_id = models.TextField(blank=True, null=True)
    object_repr = models.TextField(blank=True, null=True)  # This field type is a guess.
    action_flag = models.IntegerField()
    change_message = models.TextField(blank=True, null=True)
    content_type = models.ForeignKey('DjangoContentType', models.DO_NOTHING, blank=True, null=True)
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'django_admin_log'


class DjangoContentType(models.Model):
    id = models.IntegerField(primary_key=True)  # AutoField?
    app_label = models.TextField(blank=True, null=True)  # This field type is a guess.
    model = models.TextField(blank=True, null=True)  # This field type is a guess.

    class Meta:
        managed = False
        db_table = 'django_content_type'
        unique_together = (('app_label', 'model'),)


class DjangoMigrations(models.Model):
    id = models.IntegerField(primary_key=True)  # AutoField?
    app = models.TextField(blank=True, null=True)  # This field type is a guess.
    name = models.TextField(blank=True, null=True)  # This field type is a guess.
    applied = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_migrations'


class DjangoSession(models.Model):
    session_key = models.TextField(primary_key=True)  # This field type is a guess.
    session_data = models.TextField(blank=True, null=True)
    expire_date = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_session'


class Documentos(models.Model):
    codigo = models.CharField(max_length=30, blank=True, null=True)
    descripcion = models.CharField(max_length=90, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'documentos'


class DocumentosBancarios(models.Model):
    serie = models.CharField(max_length=75, blank=True, null=True)
    nro_documento = models.BigIntegerField(blank=True, null=True)
    id_banco = models.BigIntegerField(blank=True, null=True)
    fecha_input = models.CharField(max_length=45, blank=True, null=True)
    fecha_cobro = models.CharField(max_length=45, blank=True, null=True)
    monto = models.BigIntegerField(blank=True, null=True)
    banco = models.CharField(max_length=150, blank=True, null=True)
    id_comprobante = models.BigIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'documentos_bancarios'


class DocumentosPago(models.Model):
    codigo = models.CharField(max_length=3, blank=True, null=True)
    descripcion = models.CharField(max_length=45, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'documentos_pago'


class FactElecTipodocref(models.Model):
    estado = models.FloatField(blank=True, null=True)
    codigo_ref = models.FloatField(blank=True, null=True)
    descripcion = models.CharField(max_length=300, blank=True, null=True)
    numero = models.BigIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'fact_elec_tipodocref'


class FacturaCuenta(models.Model):
    id_factura = models.IntegerField(blank=True, null=True)
    codgrupo = models.IntegerField(blank=True, null=True)
    codclase = models.IntegerField(blank=True, null=True)
    codigo = models.BigIntegerField(blank=True, null=True)
    digito = models.CharField(max_length=3, blank=True, null=True)
    coditem = models.CharField(max_length=6, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'factura_cuenta'


class FacturaExentaGlosa(models.Model):
    id_factura = models.BigIntegerField(blank=True, null=True)
    glosa = models.CharField(max_length=1200, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'factura_exenta_glosa'


class Facturas(models.Model):
    numero = models.BigIntegerField(blank=True, null=True)
    rut = models.BigIntegerField(blank=True, null=True)
    digito = models.CharField(max_length=3, blank=True, null=True)
    nombre = models.CharField(max_length=900, blank=True, null=True)
    direccion = models.CharField(max_length=450, blank=True, null=True)
    ciudad = models.CharField(max_length=60, blank=True, null=True)
    giro = models.CharField(max_length=1500, blank=True, null=True)
    telefono = models.CharField(max_length=90, blank=True, null=True)
    agente = models.CharField(max_length=450, blank=True, null=True)
    fecha_emision = models.DateField(blank=True, null=True)
    fecha_vence = models.CharField(max_length=36, blank=True, null=True)
    fecha_pago = models.CharField(max_length=36, blank=True, null=True)
    total = models.FloatField(blank=True, null=True)
    exenta_iva = models.FloatField(blank=True, null=True)
    iva = models.FloatField(blank=True, null=True)
    descuento = models.FloatField(blank=True, null=True)
    estado = models.BigIntegerField(blank=True, null=True)
    notasventa = models.FloatField(blank=True, null=True)
    recaudador = models.CharField(max_length=60, blank=True, null=True)
    monto_iva = models.FloatField(blank=True, null=True)
    pago_efectivo = models.FloatField(blank=True, null=True)
    id_user = models.BigIntegerField(blank=True, null=True)
    is_reparto = models.BigIntegerField(blank=True, null=True)
    ordencompra = models.CharField(max_length=60, blank=True, null=True)
    num_compra = models.CharField(max_length=60, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'facturas'


class FacturasAgente(models.Model):
    numero = models.IntegerField(blank=True, null=True)
    id_cliente = models.BigIntegerField(blank=True, null=True)
    fecha_emision = models.CharField(max_length=36, blank=True, null=True)
    valor_normal = models.IntegerField(blank=True, null=True)
    valor_domingo = models.IntegerField(blank=True, null=True)
    ejemplares_semana = models.IntegerField(blank=True, null=True)
    ejemplares_domingo = models.IntegerField(blank=True, null=True)
    iva = models.IntegerField(blank=True, null=True)
    total_factura = models.IntegerField(blank=True, null=True)
    descuento = models.BigIntegerField(blank=True, null=True)
    glosa_remesa = models.CharField(max_length=750, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'facturas_agente'


class FacturasDocumentos(models.Model):
    id_factura = models.CharField(max_length=30, blank=True, null=True)
    id_tipo_docto = models.CharField(max_length=3, blank=True, null=True)
    serie = models.CharField(max_length=150, blank=True, null=True)
    nro_documento = models.CharField(max_length=21, blank=True, null=True)
    fecha_input = models.CharField(max_length=45, blank=True, null=True)
    monto = models.CharField(max_length=45, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'facturas_documentos'


class FacturasPagos(models.Model):
    fecha = models.DateField(blank=True, null=True)
    id_factura = models.FloatField(blank=True, null=True)
    id_notaventa = models.FloatField(blank=True, null=True)
    tiene_nota = models.CharField(max_length=3, blank=True, null=True)
    es_reparto = models.CharField(max_length=3, blank=True, null=True)
    exenta = models.FloatField(blank=True, null=True)
    estado = models.BigIntegerField(blank=True, null=True)
    efectivo = models.FloatField(blank=True, null=True)
    documentos = models.CharField(max_length=33, blank=True, null=True)
    total_pago = models.FloatField(blank=True, null=True)
    id_user = models.BigIntegerField(blank=True, null=True)
    formapago = models.FloatField(blank=True, null=True)
    tipo_pago = models.CharField(max_length=60, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'facturas_pagos'


class Guiacontrata(models.Model):
    id_inscripcion = models.CharField(primary_key=True, max_length=18)
    rut = models.CharField(max_length=36, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'guiacontrata'


class Guiainscripcion(models.Model):
    num_inscripcion = models.CharField(max_length=30, blank=True, null=True)
    fecha_inscripcion = models.CharField(max_length=36, blank=True, null=True)
    fecha_inicio = models.CharField(max_length=36, blank=True, null=True)
    fecha_fin = models.CharField(max_length=36, blank=True, null=True)
    id_guiaprotipo = models.CharField(max_length=3, blank=True, null=True)
    id_agente = models.CharField(max_length=6, blank=True, null=True)
    palabrasextras = models.CharField(max_length=3, blank=True, null=True)
    montopalabrasextras = models.CharField(max_length=9, blank=True, null=True)
    montoweb = models.CharField(max_length=12, blank=True, null=True)
    valorcontrato = models.CharField(max_length=30, blank=True, null=True)
    estado = models.CharField(max_length=3, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'guiainscripcion'


class GuiainscripcionProccess(models.Model):
    id_inscripcion = models.CharField(max_length=30, blank=True, null=True)
    fecha_inscripcion = models.CharField(max_length=30, blank=True, null=True)
    fecha_inicio = models.CharField(max_length=30, blank=True, null=True)
    fecha_fin = models.CharField(max_length=30, blank=True, null=True)
    fecha_proceso = models.CharField(max_length=30, blank=True, null=True)
    movimiento = models.CharField(max_length=3, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'guiainscripcion_proccess'


class Guiaprotipo(models.Model):
    codigo = models.CharField(max_length=3, blank=True, null=True)
    descripcion = models.CharField(max_length=75, blank=True, null=True)
    palabras = models.CharField(max_length=75, blank=True, null=True)
    adicional = models.CharField(max_length=75, blank=True, null=True)
    web = models.CharField(max_length=12, blank=True, null=True)
    valor = models.CharField(max_length=15, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'guiaprotipo'


class HistoriaOp(models.Model):
    numero = models.BigIntegerField(blank=True, null=True)
    fecha = models.DateField(blank=True, null=True)
    detalle = models.CharField(max_length=450, blank=True, null=True)
    cod_user = models.BigIntegerField(blank=True, null=True)
    id_op = models.BigIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'historia_op'


class InfCajadia(models.Model):
    bol_avisos = models.BigIntegerField(blank=True, null=True)
    bol_suscrip = models.BigIntegerField(blank=True, null=True)
    bol_ejemp = models.BigIntegerField(blank=True, null=True)
    bol_total = models.BigIntegerField(blank=True, null=True)
    fac_suscrip = models.BigIntegerField(blank=True, null=True)
    fac_avisos = models.BigIntegerField(blank=True, null=True)
    fac_exentas = models.BigIntegerField(blank=True, null=True)
    fac_ejemplares = models.BigIntegerField(blank=True, null=True)
    fac_directa = models.BigIntegerField(blank=True, null=True)
    fac_abonos = models.BigIntegerField(blank=True, null=True)
    fac_total = models.BigIntegerField(blank=True, null=True)
    ing_reparto = models.BigIntegerField(blank=True, null=True)
    ing_otros = models.BigIntegerField(blank=True, null=True)
    ing_total = models.BigIntegerField(blank=True, null=True)
    total_dia = models.BigIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'inf_cajadia'


class InfCajadocumentos(models.Model):
    tipo = models.CharField(max_length=60, blank=True, null=True)
    fecha = models.CharField(max_length=60, blank=True, null=True)
    numero = models.CharField(max_length=60, blank=True, null=True)
    banco = models.CharField(max_length=300, blank=True, null=True)
    valor = models.BigIntegerField(blank=True, null=True)
    cliente = models.CharField(max_length=450, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'inf_cajadocumentos'


class InfCartolacliente(models.Model):
    id_factura = models.IntegerField(blank=True, null=True)
    tipo_docto = models.CharField(max_length=150, blank=True, null=True)
    id_docto = models.IntegerField(blank=True, null=True)
    fecha = models.DateField(blank=True, null=True)
    cargo = models.IntegerField(blank=True, null=True)
    abono = models.IntegerField(blank=True, null=True)
    pendiente = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'inf_cartolacliente'


class InfFacturasasignadasrecaudado(models.Model):
    id_factura = models.IntegerField(blank=True, null=True)
    rut = models.IntegerField(blank=True, null=True)
    digito = models.CharField(max_length=3, blank=True, null=True)
    nombre = models.CharField(max_length=300, blank=True, null=True)
    fecha_emision = models.DateField(blank=True, null=True)
    fecha_vence = models.DateField(blank=True, null=True)
    monto = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'inf_facturasasignadasrecaudado'


class InfFacturasimpagasagente(models.Model):
    id_factura = models.IntegerField(blank=True, null=True)
    rut = models.IntegerField(blank=True, null=True)
    digito = models.CharField(max_length=3, blank=True, null=True)
    nombre = models.CharField(max_length=300, blank=True, null=True)
    fecha = models.DateField(blank=True, null=True)
    total = models.IntegerField(blank=True, null=True)
    pendiente = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'inf_facturasimpagasagente'


class InfFacturasimpagasrecaudador(models.Model):
    id_factura = models.IntegerField(blank=True, null=True)
    rut = models.IntegerField(blank=True, null=True)
    digito = models.CharField(max_length=3, blank=True, null=True)
    nombre = models.CharField(max_length=450, blank=True, null=True)
    direccion = models.CharField(max_length=450, blank=True, null=True)
    telefono = models.IntegerField(blank=True, null=True)
    fecha = models.DateField(blank=True, null=True)
    total = models.IntegerField(blank=True, null=True)
    pendiente = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'inf_facturasimpagasrecaudador'


class InfFacturaspagadas(models.Model):
    rut = models.CharField(max_length=45, blank=True, null=True)
    cliente = models.CharField(max_length=1500, blank=True, null=True)
    monto = models.BigIntegerField(blank=True, null=True)
    glosa = models.CharField(max_length=1500, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'inf_facturaspagadas'


class InfFacturaspagadascliente(models.Model):
    id_factura = models.FloatField(blank=True, null=True)
    fecha_emision = models.DateField(blank=True, null=True)
    fecha_pago = models.DateField(blank=True, null=True)
    total = models.FloatField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'inf_facturaspagadascliente'


class InfFacturaspagasagente(models.Model):
    id_factura = models.IntegerField(blank=True, null=True)
    rut = models.CharField(max_length=60, blank=True, null=True)
    nombre = models.CharField(max_length=300, blank=True, null=True)
    fecha_emision = models.DateField(blank=True, null=True)
    fecha_pago = models.DateField(blank=True, null=True)
    pago = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'inf_facturaspagasagente'


class InfFacturaspagasrecaudador(models.Model):
    id_factura = models.IntegerField(blank=True, null=True)
    rut = models.CharField(max_length=60, blank=True, null=True)
    nombre = models.CharField(max_length=450, blank=True, null=True)
    fecha_emision = models.DateField(blank=True, null=True)
    fecha_pago = models.DateField(blank=True, null=True)
    pago = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'inf_facturaspagasrecaudador'


class InfFacturaspendientescliente(models.Model):
    id_factura = models.FloatField(blank=True, null=True)
    fecha_emision = models.DateField(blank=True, null=True)
    total = models.IntegerField(blank=True, null=True)
    abonos = models.IntegerField(blank=True, null=True)
    pendiente = models.CharField(max_length=30, blank=True, null=True)
    id_actividad = models.FloatField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'inf_facturaspendientescliente'


class InfGuias(models.Model):
    fecha_proceso = models.CharField(max_length=36, blank=True, null=True)
    id_inscripcion = models.CharField(max_length=30, blank=True, null=True)
    nombre = models.CharField(max_length=300, blank=True, null=True)
    rut = models.CharField(max_length=36, blank=True, null=True)
    fecha_inicio = models.CharField(max_length=36, blank=True, null=True)
    fecha_fin = models.CharField(max_length=36, blank=True, null=True)
    notaventa = models.CharField(max_length=30, blank=True, null=True)
    documento = models.CharField(max_length=30, blank=True, null=True)
    tipo_guia = models.CharField(max_length=30, blank=True, null=True)
    movimiento = models.CharField(max_length=30, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'inf_guias'


class InfGuiasvigentes(models.Model):
    id_inscripcion = models.IntegerField(blank=True, null=True)
    nombre = models.CharField(max_length=450, blank=True, null=True)
    rut = models.CharField(max_length=36, blank=True, null=True)
    fecha_inicio = models.DateField(blank=True, null=True)
    fecha_fin = models.DateField(blank=True, null=True)
    notaventa = models.IntegerField(blank=True, null=True)
    documento = models.IntegerField(blank=True, null=True)
    tipo_guia = models.CharField(max_length=60, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'inf_guiasvigentes'


class InfListadoboletas(models.Model):
    id_boleta = models.FloatField(blank=True, null=True)
    rut = models.CharField(max_length=36, blank=True, null=True)
    nombre = models.CharField(max_length=300, blank=True, null=True)
    valor = models.FloatField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'inf_listadoboletas'


class InfListadofacturas(models.Model):
    id_factura = models.FloatField(blank=True, null=True)
    rut = models.CharField(max_length=36, blank=True, null=True)
    nombre = models.CharField(max_length=300, blank=True, null=True)
    valor = models.FloatField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'inf_listadofacturas'


class InfListadofacturaspendientes(models.Model):
    nombre = models.CharField(max_length=450, blank=True, null=True)
    rut = models.FloatField(blank=True, null=True)
    digito = models.CharField(max_length=3, blank=True, null=True)
    id_factura = models.FloatField(blank=True, null=True)
    fecha_emision = models.DateField(blank=True, null=True)
    agente = models.CharField(max_length=150, blank=True, null=True)
    recaudador = models.CharField(max_length=150, blank=True, null=True)
    valor = models.IntegerField(blank=True, null=True)
    abonos = models.IntegerField(blank=True, null=True)
    saldo = models.IntegerField(blank=True, null=True)
    direccion = models.CharField(max_length=450, blank=True, null=True)
    telefono = models.CharField(max_length=60, blank=True, null=True)
    ciudad = models.CharField(max_length=60, blank=True, null=True)
    feche_vence = models.DateField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'inf_listadofacturaspendientes'


class InfNotaspendientes(models.Model):
    id_nota = models.IntegerField(blank=True, null=True)
    documento = models.CharField(max_length=150, blank=True, null=True)
    fecha = models.DateField(blank=True, null=True)
    rut = models.CharField(max_length=48, blank=True, null=True)
    nombre = models.CharField(max_length=450, blank=True, null=True)
    glosa = models.CharField(max_length=1500, blank=True, null=True)
    valor = models.BigIntegerField(blank=True, null=True)
    canje = models.CharField(max_length=300, blank=True, null=True)
    fechapublicacion = models.CharField(max_length=450, blank=True, null=True)
    agente = models.CharField(max_length=300, blank=True, null=True)
    tipo = models.CharField(max_length=300, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'inf_notaspendientes'


class InfSuscripciondetalle(models.Model):
    id_suscripcion = models.BigIntegerField(blank=True, null=True)
    rut = models.CharField(max_length=36, blank=True, null=True)
    nombre = models.CharField(max_length=300, blank=True, null=True)
    documento = models.CharField(max_length=45, blank=True, null=True)
    desde = models.CharField(max_length=36, blank=True, null=True)
    hasta = models.CharField(max_length=36, blank=True, null=True)
    tipo = models.CharField(max_length=3, blank=True, null=True)
    dias = models.CharField(max_length=9, blank=True, null=True)
    ventabruto = models.CharField(max_length=15, blank=True, null=True)
    ventaneto = models.CharField(max_length=15, blank=True, null=True)
    valorunitario = models.CharField(max_length=9, blank=True, null=True)
    valorps = models.CharField(max_length=15, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'inf_suscripciondetalle'


class InfSuscripcionesvencidas(models.Model):
    id_suscripcion = models.BigIntegerField(blank=True, null=True)
    rut = models.CharField(max_length=36, blank=True, null=True)
    nombre = models.CharField(max_length=450, blank=True, null=True)
    hasta = models.DateField(blank=True, null=True)
    tipo = models.CharField(max_length=150, blank=True, null=True)
    direccion = models.CharField(max_length=450, blank=True, null=True)
    ciudad = models.CharField(max_length=150, blank=True, null=True)
    agente = models.CharField(max_length=210, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'inf_suscripcionesvencidas'


class InfSuscripcionresumen(models.Model):
    trimestral = models.CharField(max_length=6, blank=True, null=True)
    trimestralps = models.CharField(max_length=18, blank=True, null=True)
    semestral = models.CharField(max_length=6, blank=True, null=True)
    semestralps = models.CharField(max_length=18, blank=True, null=True)
    anual = models.CharField(max_length=9, blank=True, null=True)
    anualps = models.CharField(max_length=21, blank=True, null=True)
    parcial = models.CharField(max_length=3, blank=True, null=True)
    parcialps = models.CharField(max_length=3, blank=True, null=True)
    menos_30 = models.CharField(max_length=6, blank=True, null=True)
    entre_30_60 = models.CharField(max_length=9, blank=True, null=True)
    mas_60 = models.CharField(max_length=6, blank=True, null=True)
    historicas = models.CharField(max_length=12, blank=True, null=True)
    anticipos = models.CharField(max_length=3, blank=True, null=True)
    anticiposps = models.CharField(max_length=15, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'inf_suscripcionresumen'


class InfSuscriptoresporvencer(models.Model):
    id_suscripcion = models.BigIntegerField(blank=True, null=True)
    rut = models.CharField(max_length=36, blank=True, null=True)
    nombre = models.CharField(max_length=450, blank=True, null=True)
    hasta = models.DateField(blank=True, null=True)
    tipo = models.CharField(max_length=150, blank=True, null=True)
    direccion = models.CharField(max_length=450, blank=True, null=True)
    ciudad = models.CharField(max_length=450, blank=True, null=True)
    agente = models.CharField(max_length=300, blank=True, null=True)
    correo = models.CharField(max_length=300, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'inf_suscriptoresporvencer'


class InfSuscriptoresvigentes(models.Model):
    id_suscripcion = models.BigIntegerField(blank=True, null=True)
    rut = models.CharField(max_length=36, blank=True, null=True)
    nombre = models.CharField(max_length=450, blank=True, null=True)
    desde = models.CharField(max_length=36, blank=True, null=True)
    hasta = models.CharField(max_length=36, blank=True, null=True)
    direccion = models.CharField(max_length=450, blank=True, null=True)
    ciudad = models.CharField(max_length=60, blank=True, null=True)
    tipo = models.CharField(max_length=60, blank=True, null=True)
    personal = models.CharField(max_length=60, blank=True, null=True)
    n = models.CharField(max_length=60, blank=True, null=True)
    vd = models.CharField(max_length=60, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'inf_suscriptoresvigentes'


class Infoprinters(models.Model):
    status_printer_boleta = models.CharField(max_length=3, blank=True, null=True)
    numero_boleta = models.CharField(max_length=3, blank=True, null=True)
    status_printer_factura = models.CharField(max_length=3, blank=True, null=True)
    numero_factura = models.CharField(max_length=3, blank=True, null=True)
    status_printer_comprobante = models.CharField(max_length=3, blank=True, null=True)
    numero_comprobante = models.CharField(max_length=3, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'infoprinters'


class Itemsboleta(models.Model):
    id_boleta = models.BigIntegerField(blank=True, null=True)
    num_item = models.CharField(max_length=3, blank=True, null=True)
    unidades = models.BigIntegerField(blank=True, null=True)
    detalle = models.CharField(max_length=450, blank=True, null=True)
    p_unitario = models.BigIntegerField(blank=True, null=True)
    valor = models.BigIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'itemsboleta'


class Itemsfactura(models.Model):
    id_factura = models.IntegerField(blank=True, null=True)
    id_item = models.BigIntegerField(blank=True, null=True)
    cantidad = models.CharField(max_length=60, blank=True, null=True)
    detalle = models.CharField(max_length=450, blank=True, null=True)
    precio_unit = models.IntegerField(blank=True, null=True)
    total_item = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'itemsfactura'


class Itemsnotacredito(models.Model):
    id_nota = models.IntegerField(blank=True, null=True)
    id_item = models.IntegerField(blank=True, null=True)
    detalle = models.CharField(max_length=450, blank=True, null=True)
    monto_item = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'itemsnotacredito'


class Itemsnotadebito(models.Model):
    id_nota = models.IntegerField(blank=True, null=True)
    id_item = models.IntegerField(blank=True, null=True)
    item = models.CharField(max_length=450, blank=True, null=True)
    precio = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'itemsnotadebito'


class Itemsnotaventa(models.Model):
    id_nota = models.BigIntegerField(blank=True, null=True)
    num_item = models.BigIntegerField(blank=True, null=True)
    item = models.CharField(max_length=450, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'itemsnotaventa'


class Itemsotrosingresos(models.Model):
    id_ingreso = models.CharField(max_length=12, blank=True, null=True)
    num_item = models.CharField(max_length=3, blank=True, null=True)
    detalle = models.CharField(max_length=450, blank=True, null=True)
    valor = models.BigIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'itemsotrosingresos'


class Log(models.Model):
    id_estadistica = models.CharField(max_length=60, blank=True, null=True)
    n1 = models.CharField(max_length=300, blank=True, null=True)
    entrada = models.BigIntegerField(blank=True, null=True)
    fecha = models.DateField(blank=True, null=True)
    id_user = models.BigIntegerField(blank=True, null=True)
    detalle = models.CharField(max_length=300, blank=True, null=True)
    rut_cliente = models.CharField(max_length=60, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'log'


class MdCatalogs(models.Model):
    id = models.FloatField(primary_key=True)
    connection_id_fk = models.ForeignKey('MdConnections', models.DO_NOTHING, db_column='connection_id_fk')
    catalog_name = models.CharField(max_length=12000, blank=True, null=True)
    dummy_flag = models.CharField(max_length=3, blank=True, null=True)
    native_sql = models.TextField(blank=True, null=True)
    native_key = models.CharField(max_length=12000, blank=True, null=True)
    comments = models.CharField(max_length=12000, blank=True, null=True)
    security_group_id = models.FloatField()
    created_on = models.DateField()
    created_by = models.CharField(max_length=765, blank=True, null=True)
    last_updated_on = models.DateField(blank=True, null=True)
    last_updated_by = models.CharField(max_length=765, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'md_catalogs'


class MdColumns(models.Model):
    id = models.FloatField(primary_key=True)
    table_id_fk = models.ForeignKey('MdTables', models.DO_NOTHING, db_column='table_id_fk')
    column_name = models.CharField(max_length=12000)
    column_order = models.FloatField()
    column_type = models.CharField(max_length=12000, blank=True, null=True)
    precision = models.FloatField(blank=True, null=True)
    scale = models.FloatField(blank=True, null=True)
    nullable = models.CharField(max_length=3, blank=True, null=True)
    default_value = models.CharField(max_length=12000, blank=True, null=True)
    native_sql = models.TextField(blank=True, null=True)
    native_key = models.CharField(max_length=12000, blank=True, null=True)
    datatype_transformed_flag = models.CharField(max_length=3, blank=True, null=True)
    comments = models.CharField(max_length=12000, blank=True, null=True)
    security_group_id = models.FloatField()
    created_by = models.CharField(max_length=765, blank=True, null=True)
    created_on = models.DateField()
    last_updated_by = models.CharField(max_length=765, blank=True, null=True)
    last_updated_on = models.DateField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'md_columns'


class MdConnections(models.Model):
    id = models.FloatField(primary_key=True)
    project_id_fk = models.ForeignKey('MdProjects', models.DO_NOTHING, db_column='project_id_fk')
    type = models.CharField(max_length=12000, blank=True, null=True)
    host = models.CharField(max_length=12000, blank=True, null=True)
    port = models.FloatField(blank=True, null=True)
    username = models.CharField(max_length=12000, blank=True, null=True)
    password = models.CharField(max_length=12000, blank=True, null=True)
    dburl = models.CharField(max_length=12000, blank=True, null=True)
    name = models.CharField(max_length=765, blank=True, null=True)
    native_sql = models.TextField(blank=True, null=True)
    num_catalogs = models.FloatField(blank=True, null=True)
    num_columns = models.FloatField(blank=True, null=True)
    num_constraints = models.FloatField(blank=True, null=True)
    num_groups = models.FloatField(blank=True, null=True)
    num_roles = models.FloatField(blank=True, null=True)
    num_indexes = models.FloatField(blank=True, null=True)
    num_other_objects = models.FloatField(blank=True, null=True)
    num_packages = models.FloatField(blank=True, null=True)
    num_privileges = models.FloatField(blank=True, null=True)
    num_schemas = models.FloatField(blank=True, null=True)
    num_sequences = models.FloatField(blank=True, null=True)
    num_stored_programs = models.FloatField(blank=True, null=True)
    num_synonyms = models.FloatField(blank=True, null=True)
    num_tables = models.FloatField(blank=True, null=True)
    num_tablespaces = models.FloatField(blank=True, null=True)
    num_triggers = models.FloatField(blank=True, null=True)
    num_user_defined_data_types = models.FloatField(blank=True, null=True)
    num_users = models.FloatField(blank=True, null=True)
    num_views = models.FloatField(blank=True, null=True)
    comments = models.CharField(max_length=12000, blank=True, null=True)
    security_group_id = models.FloatField()
    created_on = models.DateField()
    created_by = models.CharField(max_length=765, blank=True, null=True)
    last_updated_on = models.DateField(blank=True, null=True)
    last_updated_by = models.CharField(max_length=765, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'md_connections'


class MdConstraintDetails(models.Model):
    id = models.FloatField(primary_key=True)
    ref_flag = models.CharField(max_length=3, blank=True, null=True)
    constraint_id_fk = models.ForeignKey('MdConstraints', models.DO_NOTHING, db_column='constraint_id_fk')
    column_id_fk = models.ForeignKey(MdColumns, models.DO_NOTHING, db_column='column_id_fk', blank=True, null=True)
    column_portion = models.FloatField(blank=True, null=True)
    constraint_text = models.TextField(blank=True, null=True)
    detail_order = models.FloatField()
    security_group_id = models.FloatField()
    created_on = models.DateField()
    created_by = models.CharField(max_length=765, blank=True, null=True)
    last_updated_on = models.DateField(blank=True, null=True)
    last_updated_by = models.CharField(max_length=765, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'md_constraint_details'


class MdConstraints(models.Model):
    id = models.FloatField(primary_key=True)
    delete_clause = models.CharField(max_length=12000, blank=True, null=True)
    name = models.CharField(max_length=12000, blank=True, null=True)
    constraint_type = models.CharField(max_length=12000, blank=True, null=True)
    table_id_fk = models.ForeignKey('MdTables', models.DO_NOTHING, db_column='table_id_fk')
    reftable_id_fk = models.FloatField(blank=True, null=True)
    constraint_text = models.TextField(blank=True, null=True)
    language = models.CharField(max_length=120)
    comments = models.CharField(max_length=12000, blank=True, null=True)
    security_group_id = models.FloatField()
    created_on = models.DateField()
    created_by = models.CharField(max_length=765, blank=True, null=True)
    last_updated_on = models.DateField(blank=True, null=True)
    last_updated_by = models.CharField(max_length=765, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'md_constraints'


class MdDerivatives(models.Model):
    id = models.FloatField(primary_key=True)
    src_id = models.FloatField()
    src_type = models.CharField(max_length=12000, blank=True, null=True)
    derived_id = models.FloatField()
    derived_type = models.CharField(max_length=12000, blank=True, null=True)
    derived_connection_id_fk = models.ForeignKey(MdConnections, models.DO_NOTHING, db_column='derived_connection_id_fk')
    transformed = models.CharField(max_length=3, blank=True, null=True)
    original_identifier = models.CharField(max_length=12000, blank=True, null=True)
    new_identifier = models.CharField(max_length=12000, blank=True, null=True)
    derived_object_namespace = models.CharField(max_length=120, blank=True, null=True)
    derivative_reason = models.CharField(max_length=30, blank=True, null=True)
    security_group_id = models.FloatField()
    created_on = models.DateField()
    created_by = models.CharField(max_length=765, blank=True, null=True)
    last_updated_on = models.DateField(blank=True, null=True)
    last_updated_by = models.CharField(max_length=765, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'md_derivatives'


class MdGroupMembers(models.Model):
    id = models.FloatField(primary_key=True)
    group_id_fk = models.ForeignKey('MdGroups', models.DO_NOTHING, db_column='group_id_fk')
    user_id_fk = models.ForeignKey('MdUsers', models.DO_NOTHING, db_column='user_id_fk', blank=True, null=True)
    group_member_id_fk = models.ForeignKey('MdGroups', models.DO_NOTHING, db_column='group_member_id_fk', blank=True, null=True)
    security_group_id = models.FloatField()
    created_on = models.DateField()
    created_by = models.CharField(max_length=765, blank=True, null=True)
    last_updated_on = models.DateField(blank=True, null=True)
    last_updated_by = models.CharField(max_length=765, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'md_group_members'


class MdGroupPrivileges(models.Model):
    id = models.FloatField(primary_key=True)
    group_id_fk = models.ForeignKey('MdGroups', models.DO_NOTHING, db_column='group_id_fk')
    privilege_id_fk = models.ForeignKey('MdPrivileges', models.DO_NOTHING, db_column='privilege_id_fk')
    security_group_id = models.FloatField()
    created_on = models.DateField()
    created_by = models.CharField(max_length=765, blank=True, null=True)
    last_updated_on = models.DateField(blank=True, null=True)
    last_updated_by = models.CharField(max_length=765, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'md_group_privileges'


class MdGroups(models.Model):
    id = models.FloatField(primary_key=True)
    schema_id_fk = models.ForeignKey('MdSchemas', models.DO_NOTHING, db_column='schema_id_fk')
    group_name = models.CharField(max_length=12000, blank=True, null=True)
    group_flag = models.CharField(max_length=3, blank=True, null=True)
    native_sql = models.TextField(blank=True, null=True)
    native_key = models.CharField(max_length=12000, blank=True, null=True)
    comments = models.CharField(max_length=12000, blank=True, null=True)
    security_group_id = models.FloatField()
    created_on = models.DateField()
    created_by = models.CharField(max_length=765, blank=True, null=True)
    last_updated_on = models.DateField(blank=True, null=True)
    last_updated_by = models.CharField(max_length=765, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'md_groups'


class MdIndexDetails(models.Model):
    id = models.FloatField(primary_key=True)
    index_id_fk = models.ForeignKey('MdIndexes', models.DO_NOTHING, db_column='index_id_fk')
    column_id_fk = models.ForeignKey(MdColumns, models.DO_NOTHING, db_column='column_id_fk')
    index_portion = models.FloatField(blank=True, null=True)
    detail_order = models.FloatField()
    security_group_id = models.FloatField()
    created_on = models.DateField()
    created_by = models.CharField(max_length=765, blank=True, null=True)
    last_updated_on = models.DateField(blank=True, null=True)
    last_updated_by = models.CharField(max_length=765, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'md_index_details'


class MdIndexes(models.Model):
    id = models.FloatField(primary_key=True)
    index_type = models.CharField(max_length=12000, blank=True, null=True)
    table_id_fk = models.ForeignKey('MdTables', models.DO_NOTHING, db_column='table_id_fk')
    index_name = models.CharField(max_length=12000, blank=True, null=True)
    native_sql = models.TextField(blank=True, null=True)
    native_key = models.CharField(max_length=12000, blank=True, null=True)
    comments = models.CharField(max_length=12000, blank=True, null=True)
    security_group_id = models.FloatField()
    created_on = models.DateField()
    created_by = models.CharField(max_length=765, blank=True, null=True)
    last_updated_on = models.DateField(blank=True, null=True)
    last_updated_by = models.CharField(max_length=12000, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'md_indexes'


class MdMigrDependency(models.Model):
    id = models.FloatField(primary_key=True)
    connection_id_fk = models.ForeignKey(MdConnections, models.DO_NOTHING, db_column='connection_id_fk')
    parent_id = models.FloatField()
    child_id = models.FloatField()
    parent_object_type = models.CharField(max_length=12000)
    child_object_type = models.CharField(max_length=12000)
    security_group_id = models.FloatField()
    created_on = models.DateField()
    created_by = models.CharField(max_length=765, blank=True, null=True)
    last_updated_on = models.DateField(blank=True, null=True)
    last_updated_by = models.CharField(max_length=765, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'md_migr_dependency'


class MdMigrParameter(models.Model):
    id = models.FloatField(primary_key=True)
    connection_id_fk = models.ForeignKey(MdConnections, models.DO_NOTHING, db_column='connection_id_fk')
    object_id = models.FloatField()
    object_type = models.CharField(max_length=12000)
    param_existing = models.FloatField()
    param_order = models.FloatField()
    param_name = models.CharField(max_length=12000)
    param_type = models.CharField(max_length=12000)
    param_data_type = models.CharField(max_length=12000)
    percision = models.FloatField(blank=True, null=True)
    scale = models.FloatField(blank=True, null=True)
    nullable = models.CharField(max_length=3)
    default_value = models.CharField(max_length=12000, blank=True, null=True)
    security_group_id = models.FloatField()
    created_on = models.DateField()
    created_by = models.CharField(max_length=765, blank=True, null=True)
    last_updated_on = models.DateField(blank=True, null=True)
    last_updated_by = models.CharField(max_length=765, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'md_migr_parameter'


class MdMigrWeakdep(models.Model):
    id = models.FloatField(primary_key=True)
    connection_id_fk = models.ForeignKey(MdConnections, models.DO_NOTHING, db_column='connection_id_fk')
    schema_id_fk = models.ForeignKey('MdSchemas', models.DO_NOTHING, db_column='schema_id_fk')
    parent_id = models.FloatField()
    child_name = models.CharField(max_length=12000)
    parent_type = models.CharField(max_length=12000)
    child_type = models.CharField(max_length=12000)
    security_group_id = models.FloatField()
    created_on = models.DateField()
    created_by = models.CharField(max_length=765, blank=True, null=True)
    last_updated_on = models.DateField(blank=True, null=True)
    last_updated_by = models.CharField(max_length=765, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'md_migr_weakdep'


class MdOtherObjects(models.Model):
    id = models.FloatField(primary_key=True)
    schema_id_fk = models.ForeignKey('MdSchemas', models.DO_NOTHING, db_column='schema_id_fk')
    name = models.CharField(max_length=12000, blank=True, null=True)
    native_sql = models.TextField(blank=True, null=True)
    native_key = models.CharField(max_length=12000, blank=True, null=True)
    comments = models.CharField(max_length=12000, blank=True, null=True)
    security_group_id = models.FloatField()
    created_on = models.DateField()
    created_by = models.CharField(max_length=765, blank=True, null=True)
    last_updated_on = models.DateField(blank=True, null=True)
    last_updated_by = models.CharField(max_length=765, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'md_other_objects'


class MdPackages(models.Model):
    id = models.FloatField(primary_key=True)
    schema_id_fk = models.ForeignKey('MdSchemas', models.DO_NOTHING, db_column='schema_id_fk')
    name = models.CharField(max_length=12000)
    package_header = models.TextField(blank=True, null=True)
    native_sql = models.TextField(blank=True, null=True)
    native_key = models.CharField(max_length=12000, blank=True, null=True)
    language = models.CharField(max_length=120)
    comments = models.CharField(max_length=12000, blank=True, null=True)
    security_group_id = models.FloatField()
    created_on = models.DateField()
    created_by = models.CharField(max_length=765, blank=True, null=True)
    last_updated_on = models.DateField(blank=True, null=True)
    last_updated_by = models.CharField(max_length=765, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'md_packages'


class MdPrivileges(models.Model):
    id = models.FloatField(primary_key=True)
    schema_id_fk = models.ForeignKey('MdSchemas', models.DO_NOTHING, db_column='schema_id_fk')
    privilege_name = models.CharField(max_length=12000)
    privelege_object_id = models.FloatField(blank=True, null=True)
    privelegeobjecttype = models.CharField(max_length=12000)
    privelege_type = models.CharField(max_length=12000)
    admin_option = models.CharField(max_length=3, blank=True, null=True)
    native_sql = models.TextField()
    native_key = models.CharField(max_length=12000, blank=True, null=True)
    comments = models.CharField(max_length=12000, blank=True, null=True)
    security_group_id = models.FloatField()
    created_on = models.DateField()
    created_by = models.CharField(max_length=765, blank=True, null=True)
    last_updated_on = models.DateField(blank=True, null=True)
    last_updated_by = models.CharField(max_length=765, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'md_privileges'


class MdProjects(models.Model):
    id = models.FloatField(primary_key=True)
    project_name = models.CharField(max_length=12000)
    comments = models.CharField(max_length=12000, blank=True, null=True)
    security_group_id = models.FloatField()
    created_on = models.DateField()
    created_by = models.CharField(max_length=765, blank=True, null=True)
    last_updated_on = models.DateField(blank=True, null=True)
    last_updated_by = models.CharField(max_length=765, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'md_projects'


class MdRegistry(models.Model):
    object_type = models.CharField(max_length=90)
    object_name = models.CharField(max_length=90)

    class Meta:
        managed = False
        db_table = 'md_registry'
        unique_together = (('object_type', 'object_name'),)


class MdRepoversions(models.Model):
    revision = models.FloatField()

    class Meta:
        managed = False
        db_table = 'md_repoversions'


class MdSchemas(models.Model):
    id = models.FloatField(primary_key=True)
    catalog_id_fk = models.ForeignKey(MdCatalogs, models.DO_NOTHING, db_column='catalog_id_fk')
    name = models.CharField(max_length=12000, blank=True, null=True)
    type = models.CharField(max_length=3, blank=True, null=True)
    character_set = models.CharField(max_length=12000, blank=True, null=True)
    version_tag = models.CharField(max_length=120, blank=True, null=True)
    native_sql = models.TextField(blank=True, null=True)
    native_key = models.CharField(max_length=12000, blank=True, null=True)
    comments = models.CharField(max_length=12000, blank=True, null=True)
    security_group_id = models.FloatField()
    created_on = models.DateField()
    created_by = models.CharField(max_length=765, blank=True, null=True)
    last_updated_on = models.DateField(blank=True, null=True)
    last_updated_by = models.CharField(max_length=765, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'md_schemas'


class MdSequences(models.Model):
    id = models.FloatField(primary_key=True)
    schema_id_fk = models.ForeignKey(MdSchemas, models.DO_NOTHING, db_column='schema_id_fk')
    name = models.CharField(max_length=12000)
    seq_start = models.FloatField()
    incr = models.FloatField()
    native_sql = models.TextField(blank=True, null=True)
    native_key = models.CharField(max_length=12000, blank=True, null=True)
    comments = models.CharField(max_length=12000, blank=True, null=True)
    security_group_id = models.FloatField()
    created_on = models.DateField()
    created_by = models.CharField(max_length=765)
    last_updated_on = models.DateField(blank=True, null=True)
    last_updated_by = models.CharField(max_length=765, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'md_sequences'


class MdStoredPrograms(models.Model):
    id = models.FloatField(primary_key=True)
    schema_id_fk = models.ForeignKey(MdSchemas, models.DO_NOTHING, db_column='schema_id_fk')
    programtype = models.CharField(max_length=60, blank=True, null=True)
    name = models.CharField(max_length=12000, blank=True, null=True)
    package_id_fk = models.ForeignKey(MdPackages, models.DO_NOTHING, db_column='package_id_fk', blank=True, null=True)
    native_sql = models.TextField(blank=True, null=True)
    native_key = models.CharField(max_length=12000, blank=True, null=True)
    language = models.CharField(max_length=120)
    comments = models.CharField(max_length=12000, blank=True, null=True)
    security_group_id = models.FloatField()
    created_on = models.DateField()
    created_by = models.CharField(max_length=765, blank=True, null=True)
    last_updated_on = models.DateField(blank=True, null=True)
    last_updated_by = models.CharField(max_length=765, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'md_stored_programs'


class MdSynonyms(models.Model):
    id = models.FloatField(primary_key=True)
    schema_id_fk = models.ForeignKey(MdSchemas, models.DO_NOTHING, db_column='schema_id_fk')
    name = models.CharField(max_length=12000)
    synonym_for_id = models.FloatField()
    for_object_type = models.CharField(max_length=12000)
    private_visibility = models.CharField(max_length=3, blank=True, null=True)
    native_sql = models.TextField(blank=True, null=True)
    native_key = models.CharField(max_length=12000, blank=True, null=True)
    comments = models.CharField(max_length=12000, blank=True, null=True)
    security_group_id = models.FloatField()
    created_on = models.DateField()
    created_by = models.CharField(max_length=765, blank=True, null=True)
    last_updated_on = models.DateField(blank=True, null=True)
    last_updated_by = models.CharField(max_length=765, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'md_synonyms'


class MdTables(models.Model):
    id = models.FloatField(primary_key=True)
    schema_id_fk = models.ForeignKey(MdSchemas, models.DO_NOTHING, db_column='schema_id_fk')
    table_name = models.CharField(max_length=12000)
    native_sql = models.TextField(blank=True, null=True)
    native_key = models.CharField(max_length=12000, blank=True, null=True)
    qualified_native_name = models.CharField(max_length=12000)
    comments = models.CharField(max_length=12000, blank=True, null=True)
    security_group_id = models.FloatField()
    created_on = models.DateField()
    created_by = models.CharField(max_length=765, blank=True, null=True)
    last_updated_on = models.DateField(blank=True, null=True)
    last_updated_by = models.CharField(max_length=765, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'md_tables'


class MdTablespaces(models.Model):
    id = models.FloatField(primary_key=True)
    schema_id_fk = models.ForeignKey(MdSchemas, models.DO_NOTHING, db_column='schema_id_fk')
    tablespace_name = models.CharField(max_length=12000, blank=True, null=True)
    native_sql = models.TextField(blank=True, null=True)
    native_key = models.CharField(max_length=12000, blank=True, null=True)
    comments = models.CharField(max_length=12000, blank=True, null=True)
    security_group_id = models.FloatField()
    created_on = models.DateField()
    created_by = models.CharField(max_length=765, blank=True, null=True)
    last_updated_on = models.DateField(blank=True, null=True)
    last_updated_by = models.CharField(max_length=765, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'md_tablespaces'


class MdTriggers(models.Model):
    id = models.FloatField(primary_key=True)
    table_or_view_id_fk = models.FloatField()
    trigger_on_flag = models.CharField(max_length=3)
    trigger_name = models.CharField(max_length=12000, blank=True, null=True)
    trigger_timing = models.CharField(max_length=12000, blank=True, null=True)
    trigger_operation = models.CharField(max_length=12000, blank=True, null=True)
    trigger_event = models.CharField(max_length=12000, blank=True, null=True)
    native_sql = models.TextField(blank=True, null=True)
    native_key = models.CharField(max_length=12000, blank=True, null=True)
    language = models.CharField(max_length=120)
    comments = models.CharField(max_length=12000, blank=True, null=True)
    security_group_id = models.FloatField()
    created_on = models.DateField()
    created_by = models.CharField(max_length=765, blank=True, null=True)
    last_updated_on = models.DateField(blank=True, null=True)
    last_updated_by = models.CharField(max_length=765, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'md_triggers'


class MdUserDefinedDataTypes(models.Model):
    id = models.FloatField(primary_key=True)
    schema_id_fk = models.ForeignKey(MdSchemas, models.DO_NOTHING, db_column='schema_id_fk')
    data_type_name = models.CharField(max_length=12000)
    definition = models.CharField(max_length=12000)
    native_sql = models.TextField()
    native_key = models.CharField(max_length=12000, blank=True, null=True)
    comments = models.CharField(max_length=12000, blank=True, null=True)
    security_group_id = models.FloatField()
    created_on = models.DateField()
    created_by = models.CharField(max_length=765, blank=True, null=True)
    last_updated_on = models.DateField(blank=True, null=True)
    last_updated_by = models.CharField(max_length=765, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'md_user_defined_data_types'


class MdUserPrivileges(models.Model):
    id = models.FloatField(primary_key=True)
    user_id_fk = models.ForeignKey('MdUsers', models.DO_NOTHING, db_column='user_id_fk')
    privilege_id_fk = models.ForeignKey(MdPrivileges, models.DO_NOTHING, db_column='privilege_id_fk', blank=True, null=True)
    security_group_id = models.FloatField()
    created_on = models.DateField()
    created_by = models.CharField(max_length=765, blank=True, null=True)
    last_updated_on = models.DateField(blank=True, null=True)
    last_udpated_by = models.CharField(max_length=765, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'md_user_privileges'


class MdUsers(models.Model):
    id = models.FloatField(primary_key=True)
    schema_id_fk = models.ForeignKey(MdSchemas, models.DO_NOTHING, db_column='schema_id_fk')
    username = models.CharField(max_length=12000)
    password = models.CharField(max_length=12000, blank=True, null=True)
    native_sql = models.TextField(blank=True, null=True)
    native_key = models.CharField(max_length=12000, blank=True, null=True)
    comments = models.CharField(max_length=12000, blank=True, null=True)
    security_group_id = models.FloatField()
    created_on = models.DateField()
    created_by = models.CharField(max_length=765, blank=True, null=True)
    last_updated_on = models.DateField(blank=True, null=True)
    last_updated_by = models.CharField(max_length=765, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'md_users'


class MdViews(models.Model):
    id = models.FloatField(primary_key=True)
    schema_id_fk = models.ForeignKey(MdSchemas, models.DO_NOTHING, db_column='schema_id_fk')
    view_name = models.CharField(max_length=12000, blank=True, null=True)
    native_sql = models.TextField(blank=True, null=True)
    native_key = models.CharField(max_length=12000, blank=True, null=True)
    language = models.CharField(max_length=120)
    comments = models.CharField(max_length=12000, blank=True, null=True)
    security_group_id = models.FloatField()
    created_on = models.DateField()
    created_by = models.CharField(max_length=765, blank=True, null=True)
    last_updated_on = models.DateField(blank=True, null=True)
    last_updated_by = models.CharField(max_length=765, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'md_views'


class MigrDatatypeTransformMap(models.Model):
    id = models.FloatField(primary_key=True)
    project_id_fk = models.ForeignKey(MdProjects, models.DO_NOTHING, db_column='project_id_fk')
    map_name = models.CharField(max_length=12000, blank=True, null=True)
    security_group_id = models.FloatField()
    created_on = models.DateField()
    created_by = models.CharField(max_length=765, blank=True, null=True)
    last_updated_on = models.DateField(blank=True, null=True)
    last_updated_by = models.CharField(max_length=765, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'migr_datatype_transform_map'


class MigrDatatypeTransformRule(models.Model):
    id = models.FloatField(primary_key=True)
    map_id_fk = models.ForeignKey(MigrDatatypeTransformMap, models.DO_NOTHING, db_column='map_id_fk')
    source_data_type_name = models.CharField(max_length=12000)
    source_precision = models.FloatField(blank=True, null=True)
    source_scale = models.FloatField(blank=True, null=True)
    target_data_type_name = models.CharField(max_length=12000)
    target_precision = models.FloatField(blank=True, null=True)
    target_scale = models.FloatField(blank=True, null=True)
    security_group_id = models.FloatField()
    created_on = models.DateField()
    created_by = models.CharField(max_length=765, blank=True, null=True)
    last_updated_on = models.DateField(blank=True, null=True)
    last_updated_by = models.CharField(max_length=765, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'migr_datatype_transform_rule'


class MigrGenerationOrder(models.Model):
    id = models.FloatField(primary_key=True)
    connection_id_fk = models.ForeignKey(MdConnections, models.DO_NOTHING, db_column='connection_id_fk')
    object_id = models.FloatField()
    object_type = models.CharField(max_length=12000)
    generation_order = models.FloatField()

    class Meta:
        managed = False
        db_table = 'migr_generation_order'


class MigrationReservedWords(models.Model):
    keyword = models.CharField(max_length=120)

    class Meta:
        managed = False
        db_table = 'migration_reserved_words'


class Migrlog(models.Model):
    id = models.FloatField(primary_key=True)
    parent_log = models.ForeignKey('self', models.DO_NOTHING, blank=True, null=True)
    log_date = models.DateTimeField()
    severity = models.IntegerField()
    logtext = models.CharField(max_length=12000, blank=True, null=True)
    phase = models.CharField(max_length=60, blank=True, null=True)
    ref_object_id = models.FloatField(blank=True, null=True)
    ref_object_type = models.CharField(max_length=12000, blank=True, null=True)
    connection_id_fk = models.FloatField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'migrlog'


class NotasGlosaPromocional(models.Model):
    glosa_1 = models.CharField(max_length=150, blank=True, null=True)
    glosa_2 = models.CharField(max_length=150, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'notas_glosa_promocional'


class NotasPendientesAutorizacion(models.Model):
    id_nota = models.FloatField(blank=True, null=True)
    id_user = models.BigIntegerField(blank=True, null=True)
    id_user_autoriza = models.BigIntegerField(blank=True, null=True)
    nota_pendiente = models.CharField(max_length=42, blank=True, null=True)
    autorizada = models.CharField(max_length=30, blank=True, null=True)
    tipo = models.CharField(max_length=60, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'notas_pendientes_autorizacion'


class Notascredito(models.Model):
    numero = models.BigIntegerField(blank=True, null=True)
    id_factura = models.BigIntegerField(blank=True, null=True)
    fecha_emision = models.CharField(max_length=150, blank=True, null=True)
    iva = models.BigIntegerField(blank=True, null=True)
    monto_iva = models.BigIntegerField(blank=True, null=True)
    total = models.BigIntegerField(blank=True, null=True)
    estado = models.BigIntegerField(blank=True, null=True)
    id_user = models.BigIntegerField(blank=True, null=True)
    tipo_codref = models.BigIntegerField(blank=True, null=True)
    codref = models.BigIntegerField(blank=True, null=True)
    numerofactura = models.BigIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'notascredito'


class Notasdebito(models.Model):
    numero = models.BigIntegerField(primary_key=True)
    id_factura = models.BigIntegerField()
    fecha_emision = models.CharField(max_length=60, blank=True, null=True)
    monto_iva = models.BigIntegerField(blank=True, null=True)
    total = models.BigIntegerField(blank=True, null=True)
    estado = models.BigIntegerField(blank=True, null=True)
    id_user = models.BigIntegerField(blank=True, null=True)
    tipodocref = models.BigIntegerField(blank=True, null=True)
    tipodocumento = models.BigIntegerField(blank=True, null=True)
    numerofactura = models.BigIntegerField(blank=True, null=True)
    iva = models.FloatField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'notasdebito'


class Notasdocumentos(models.Model):
    id_notaventa = models.CharField(max_length=18, blank=True, null=True)
    id_numdocto = models.CharField(max_length=18, blank=True, null=True)
    id_tipodocto = models.CharField(max_length=3, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'notasdocumentos'


class Notasobservaciones(models.Model):
    id_nota = models.CharField(max_length=18, blank=True, null=True)
    observacion = models.CharField(max_length=600, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'notasobservaciones'


class Notasventa(models.Model):
    numero = models.BigIntegerField()
    fecha = models.DateField(blank=True, null=True)
    rut_cliente = models.CharField(max_length=60, blank=True, null=True)
    id_agente = models.BigIntegerField(blank=True, null=True)
    id_documento = models.BigIntegerField(blank=True, null=True)
    num_documento = models.BigIntegerField(blank=True, null=True)
    num_orden = models.BigIntegerField(blank=True, null=True)
    valor = models.BigIntegerField(blank=True, null=True)
    anulada = models.FloatField(blank=True, null=True)
    cancelada = models.CharField(max_length=60, blank=True, null=True)
    facturada = models.FloatField(blank=True, null=True)
    descuento = models.CharField(max_length=60, blank=True, null=True)
    total_pagar = models.CharField(max_length=60, blank=True, null=True)
    cod_avtarifa = models.CharField(max_length=60, blank=True, null=True)
    nro_pagina = models.BigIntegerField(blank=True, null=True)
    tamanocm = models.BigIntegerField(blank=True, null=True)
    columnas = models.BigIntegerField(blank=True, null=True)
    color = models.CharField(max_length=3, blank=True, null=True)
    publicacion = models.CharField(max_length=300, blank=True, null=True)
    seccion = models.CharField(max_length=600, blank=True, null=True)
    publicidad = models.CharField(max_length=60, blank=True, null=True)
    das = models.CharField(max_length=60, blank=True, null=True)
    subseccion = models.CharField(max_length=60, blank=True, null=True)
    coduser = models.CharField(max_length=60, blank=True, null=True)
    id_agencia = models.IntegerField(blank=True, null=True)
    hes_das = models.BigIntegerField(blank=True, null=True)
    direccion_factura = models.CharField(max_length=450, blank=True, null=True)
    email_factura = models.CharField(max_length=450, blank=True, null=True)
    empresa = models.CharField(max_length=60, blank=True, null=True)
    telefono_factura = models.BigIntegerField(blank=True, null=True)
    es_canje = models.IntegerField(blank=True, null=True)
    orden_agencia = models.CharField(max_length=600, blank=True, null=True)
    fechapublicacion = models.CharField(max_length=900, blank=True, null=True)


    ciudad = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'notasventa'


class Notasventappp(models.Model):
    numero = models.BigIntegerField()
    fecha = models.DateField(blank=True, null=True)
    rut_cliente = models.CharField(max_length=60, blank=True, null=True)
    id_agente = models.BigIntegerField(blank=True, null=True)
    id_documento = models.BigIntegerField(blank=True, null=True)
    num_documento = models.BigIntegerField(blank=True, null=True)
    num_orden = models.BigIntegerField(blank=True, null=True)
    valor = models.BigIntegerField(blank=True, null=True)
    anulada = models.FloatField(blank=True, null=True)
    cancelada = models.CharField(max_length=60, blank=True, null=True)
    facturada = models.FloatField(blank=True, null=True)
    descuento = models.CharField(max_length=60, blank=True, null=True)
    total_pagar = models.CharField(max_length=60, blank=True, null=True)
    cod_avtarifa = models.CharField(max_length=60, blank=True, null=True)
    nro_pagina = models.BigIntegerField(blank=True, null=True)
    tamanocm = models.BigIntegerField(blank=True, null=True)
    columnas = models.BigIntegerField(blank=True, null=True)
    color = models.CharField(max_length=3, blank=True, null=True)
    publicacion = models.CharField(max_length=300, blank=True, null=True)
    seccion = models.CharField(max_length=600, blank=True, null=True)
    publicidad = models.CharField(max_length=60, blank=True, null=True)
    das = models.CharField(max_length=60, blank=True, null=True)
    subseccion = models.CharField(max_length=60, blank=True, null=True)
    coduser = models.CharField(max_length=60, blank=True, null=True)
    id_agencia = models.IntegerField(blank=True, null=True)
    hes_das = models.BigIntegerField(blank=True, null=True)
    direccion_factura = models.CharField(max_length=450, blank=True, null=True)
    email_factura = models.CharField(max_length=450, blank=True, null=True)
    empresa = models.CharField(max_length=60, blank=True, null=True)
    telefono_factura = models.BigIntegerField(blank=True, null=True)
    es_canje = models.IntegerField(blank=True, null=True)
    orden_agencia = models.CharField(max_length=600, blank=True, null=True)
    fechapublicacion = models.CharField(max_length=900, blank=True, null=True)
    ciudad = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'notasventappp'


class Notasventavarchar(models.Model):
    numero = models.BigIntegerField(primary_key=True)
    fecha = models.DateField(blank=True, null=True)
    rut_cliente = models.CharField(max_length=60, blank=True, null=True)
    id_agente = models.CharField(max_length=60, blank=True, null=True)
    id_documento = models.BigIntegerField(blank=True, null=True)
    num_documento = models.BigIntegerField(blank=True, null=True)
    num_orden = models.BigIntegerField(blank=True, null=True)
    valor = models.BigIntegerField(blank=True, null=True)
    anulada = models.FloatField(blank=True, null=True)
    cancelada = models.CharField(max_length=60, blank=True, null=True)
    facturada = models.FloatField(blank=True, null=True)
    descuento = models.CharField(max_length=60, blank=True, null=True)
    total_pagar = models.CharField(max_length=60, blank=True, null=True)
    cod_avtarifa = models.CharField(max_length=60, blank=True, null=True)
    nro_pagina = models.BigIntegerField(blank=True, null=True)
    tamanocm = models.BigIntegerField(blank=True, null=True)
    columnas = models.BigIntegerField(blank=True, null=True)
    color = models.CharField(max_length=3, blank=True, null=True)
    publicacion = models.CharField(max_length=300, blank=True, null=True)
    seccion = models.CharField(max_length=600, blank=True, null=True)
    publicidad = models.CharField(max_length=60, blank=True, null=True)
    das = models.CharField(max_length=60, blank=True, null=True)
    subseccion = models.CharField(max_length=60, blank=True, null=True)
    coduser = models.CharField(max_length=60, blank=True, null=True)
    id_agencia = models.IntegerField(blank=True, null=True)
    hes_das = models.BigIntegerField(blank=True, null=True)
    direccion_factura = models.CharField(max_length=450, blank=True, null=True)
    email_factura = models.CharField(max_length=450, blank=True, null=True)
    empresa = models.CharField(max_length=60, blank=True, null=True)
    telefono_factura = models.BigIntegerField(blank=True, null=True)
    es_canje = models.IntegerField(blank=True, null=True)
    orden_agencia = models.CharField(max_length=600, blank=True, null=True)
    fechapublicacion = models.CharField(max_length=900, blank=True, null=True)
    ciudad = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'notasventavarchar'


class Ordentrabajo(models.Model):
    cod_ot = models.BigIntegerField()
    cod_nota_venta = models.BigIntegerField(blank=True, null=True)
    fecha = models.DateField(blank=True, null=True)
    estado_ot = models.BigIntegerField(blank=True, null=True)
    observaciones = models.CharField(max_length=1500, blank=True, null=True)
    llamadoaviso = models.CharField(max_length=1500, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'ordentrabajo'


class OtrosIngresos(models.Model):
    numero = models.BigIntegerField(blank=True, null=True)
    fecha_ingreso = models.CharField(max_length=36, blank=True, null=True)
    pagador = models.CharField(max_length=450, blank=True, null=True)
    pago_efectivo = models.CharField(max_length=21, blank=True, null=True)
    documentos = models.CharField(max_length=3, blank=True, null=True)
    reparto = models.BigIntegerField(blank=True, null=True)
    total_ingreso = models.CharField(max_length=21, blank=True, null=True)
    id_user = models.CharField(max_length=6, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'otros_ingresos'


class RepAgentes(models.Model):
    id_cliente = models.BigIntegerField()
    domicilio = models.CharField(max_length=450, blank=True, null=True)
    comuna = models.CharField(max_length=450, blank=True, null=True)
    giro = models.CharField(max_length=450, blank=True, null=True)
    descuenta = models.BigIntegerField(blank=True, null=True)
    porcentaje = models.BigIntegerField(blank=True, null=True)
    n1 = models.BigIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'rep_agentes'


class RepBoletas(models.Model):
    numero = models.BigIntegerField(blank=True, null=True)
    id_cliente = models.BigIntegerField(blank=True, null=True)
    fecha_emision = models.DateField(blank=True, null=True)
    rut_cliente = models.CharField(max_length=36, blank=True, null=True)
    nombre = models.CharField(max_length=1050, blank=True, null=True)
    valorejemplar = models.BigIntegerField(blank=True, null=True)
    ejemplares = models.BigIntegerField(blank=True, null=True)
    monto_ejemplares = models.BigIntegerField(blank=True, null=True)
    iva = models.BigIntegerField(blank=True, null=True)
    insertos = models.BigIntegerField(blank=True, null=True)
    monto_insertos = models.BigIntegerField(blank=True, null=True)
    iva_pago_insertos = models.BigIntegerField(blank=True, null=True)
    total_boleta = models.BigIntegerField(blank=True, null=True)
    tipo_boleta = models.CharField(max_length=300, blank=True, null=True)
    estado = models.BigIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'rep_boletas'


class RepBoletasAgente(models.Model):
    numero = models.BigIntegerField(blank=True, null=True)
    fecha_emision = models.DateField(blank=True, null=True)
    valor_normal = models.BigIntegerField(blank=True, null=True)
    valor_domingo = models.BigIntegerField(blank=True, null=True)
    ejemplares_semana = models.BigIntegerField(blank=True, null=True)
    ejemplares_domingo = models.BigIntegerField(blank=True, null=True)
    glosa_remesa = models.CharField(max_length=1500, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'rep_boletas_agente'


class RepBoletastemporal(models.Model):
    id = models.CharField(max_length=36, blank=True, null=True)
    numerof = models.CharField(max_length=36, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'rep_boletastemporal'


class RepClientes(models.Model):
    id_cliente = models.BigIntegerField(primary_key=True)
    rut = models.CharField(max_length=60, blank=True, null=True)
    digito = models.CharField(max_length=60, blank=True, null=True)
    nombre = models.CharField(max_length=1050, blank=True, null=True)
    direccion = models.CharField(max_length=1050, blank=True, null=True)
    telefono = models.CharField(max_length=60, blank=True, null=True)
    estado = models.BigIntegerField(blank=True, null=True)
    n1 = models.BigIntegerField(blank=True, null=True)
    n2 = models.BigIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'rep_clientes'


class RepComprobanteAgente(models.Model):
    id_comprobante = models.BigIntegerField(blank=True, null=True)
    nombre = models.CharField(max_length=450, blank=True, null=True)
    rut_cliente = models.CharField(max_length=45, blank=True, null=True)
    fecha_emision = models.CharField(max_length=60, blank=True, null=True)
    valor_normal = models.BigIntegerField(blank=True, null=True)
    valor_domingo = models.BigIntegerField(blank=True, null=True)
    ejemplares_semana = models.BigIntegerField(blank=True, null=True)
    ejemplares_domingo = models.BigIntegerField(blank=True, null=True)
    monto_insertos = models.BigIntegerField(blank=True, null=True)
    iva_pago_insertos = models.BigIntegerField(blank=True, null=True)
    glosa = models.CharField(max_length=900, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'rep_comprobante_agente'


class RepComprobantePago(models.Model):
    id_comprobante = models.BigIntegerField(blank=True, null=True)
    id_cliente = models.BigIntegerField(blank=True, null=True)
    fecha_emision = models.CharField(max_length=60, blank=True, null=True)
    rut_cliente = models.CharField(max_length=36, blank=True, null=True)
    nombre = models.CharField(max_length=600, blank=True, null=True)
    valorejemplar = models.BigIntegerField(blank=True, null=True)
    ejemplares = models.BigIntegerField(blank=True, null=True)
    monto_ejemplares = models.BigIntegerField(blank=True, null=True)
    iva = models.BigIntegerField(blank=True, null=True)
    insertos = models.BigIntegerField(blank=True, null=True)
    monto_insertos = models.BigIntegerField(blank=True, null=True)
    iva_pago_insertos = models.BigIntegerField(blank=True, null=True)
    saldo_favor = models.BigIntegerField(blank=True, null=True)
    total_comprobante = models.BigIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'rep_comprobante_pago'


class RepDatosdia(models.Model):
    ejemplares = models.IntegerField(blank=True, null=True)
    insertos = models.FloatField(blank=True, null=True)
    fecha = models.DateField(blank=True, null=True)
    hora = models.CharField(max_length=60, blank=True, null=True)
    folio = models.IntegerField(blank=True, null=True)
    paginas = models.IntegerField(blank=True, null=True)
    precio = models.IntegerField(blank=True, null=True)
    boleta = models.FloatField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'rep_datosdia'


class RepDevoluciones(models.Model):
    id_cliente = models.BigIntegerField()
    fecha_ing = models.DateField(blank=True, null=True)
    fecha_dev = models.DateField(blank=True, null=True)
    ejemplares = models.BigIntegerField(blank=True, null=True)
    estado = models.BigIntegerField(blank=True, null=True)
    glosa = models.CharField(max_length=1050, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'rep_devoluciones'


class RepEntregas(models.Model):
    id_cliente = models.BigIntegerField(blank=True, null=True)
    fecha = models.DateField(blank=True, null=True)
    ejemplares = models.BigIntegerField(blank=True, null=True)
    insertos = models.BigIntegerField(blank=True, null=True)
    valorejemplar = models.BigIntegerField(blank=True, null=True)
    estado = models.BigIntegerField(blank=True, null=True)
    descuento = models.BigIntegerField(blank=True, null=True)
    hora = models.CharField(max_length=180, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'rep_entregas'


class RepInsertosConfirmados(models.Model):
    id_confirmado = models.BigIntegerField()
    id_inserto = models.BigIntegerField(blank=True, null=True)
    empresa = models.CharField(max_length=900, blank=True, null=True)
    inserto = models.CharField(max_length=900, blank=True, null=True)
    fecha_contrato = models.DateField(blank=True, null=True)
    cantidad = models.BigIntegerField(blank=True, null=True)
    tipo_reparto = models.BigIntegerField(blank=True, null=True)
    glosa = models.CharField(max_length=900, blank=True, null=True)
    id_contratado = models.BigIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'rep_insertos_confirmados'


class RepInsertosContratados(models.Model):
    id_contratado = models.CharField(max_length=3, blank=True, null=True)
    id_inserto = models.CharField(max_length=3, blank=True, null=True)
    empresa = models.CharField(max_length=300, blank=True, null=True)
    inserto = models.CharField(max_length=129, blank=True, null=True)
    fecha_contrato = models.CharField(max_length=36, blank=True, null=True)
    cantidad = models.CharField(max_length=12, blank=True, null=True)
    tipo_reparto = models.CharField(max_length=3, blank=True, null=True)
    glosa = models.CharField(max_length=300, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'rep_insertos_contratados'


class RepInsertosTarifa(models.Model):
    id_inserto = models.CharField(max_length=3)
    descripcion = models.CharField(max_length=1500, blank=True, null=True)
    desde = models.DateField(blank=True, null=True)
    hasta = models.DateField(blank=True, null=True)
    valor = models.BigIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'rep_insertos_tarifa'


class RepParametros(models.Model):
    folio = models.CharField(max_length=15, blank=True, null=True)
    iva = models.BigIntegerField(blank=True, null=True)
    insertos = models.BigIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'rep_parametros'


class RepPrecioDiario(models.Model):
    normal = models.BigIntegerField(blank=True, null=True)
    domingo = models.BigIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'rep_precio_diario'


class RepRepartoSuscripcion(models.Model):
    fecha = models.DateField(blank=True, null=True)
    suscriptores = models.CharField(max_length=300, blank=True, null=True)
    canje = models.CharField(max_length=60, blank=True, null=True)
    personal = models.CharField(max_length=60, blank=True, null=True)
    archivo = models.CharField(max_length=60, blank=True, null=True)
    comprobantes = models.CharField(max_length=60, blank=True, null=True)
    oficio = models.CharField(max_length=60, blank=True, null=True)
    corresponsal = models.CharField(max_length=60, blank=True, null=True)
    venta_oficina = models.CharField(max_length=60, blank=True, null=True)
    otro_despacho = models.CharField(max_length=60, blank=True, null=True)
    nota_despacho = models.CharField(max_length=450, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'rep_reparto_suscripcion'


class RepSectores(models.Model):
    id_sector = models.BigIntegerField(blank=True, null=True)
    nombre = models.CharField(max_length=450, blank=True, null=True)
    calle_1 = models.CharField(max_length=450, blank=True, null=True)
    calle_2 = models.CharField(max_length=450, blank=True, null=True)
    calle_3 = models.CharField(max_length=450, blank=True, null=True)
    calle_4 = models.CharField(max_length=450, blank=True, null=True)
    ciudad = models.CharField(max_length=300, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'rep_sectores'


class RepSuplementeros(models.Model):
    id_cliente = models.BigIntegerField()
    id_sindicato = models.BigIntegerField(blank=True, null=True)
    sector = models.CharField(max_length=600, blank=True, null=True)
    saldo = models.BigIntegerField(blank=True, null=True)
    descuenta = models.BigIntegerField(blank=True, null=True)
    porcentaje = models.BigIntegerField(blank=True, null=True)
    sindicalizado = models.BigIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'rep_suplementeros'


class RepSuscripciones(models.Model):
    fecha = models.CharField(max_length=36, blank=True, null=True)
    con_cargo = models.CharField(max_length=36, blank=True, null=True)
    sin_cargo = models.CharField(max_length=36, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'rep_suscripciones'


class RepTmpBoletas(models.Model):
    id_boleta = models.BigIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'rep_tmp_boletas'


class RepTmpBoletasTraspasonap(models.Model):
    fecha = models.CharField(max_length=36, blank=True, null=True)
    inicio = models.CharField(max_length=36, blank=True, null=True)
    fin = models.CharField(max_length=36, blank=True, null=True)
    monto = models.CharField(max_length=36, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'rep_tmp_boletas_traspasonap'


class RepTmpDevoluciones(models.Model):
    id_cliente = models.CharField(max_length=36, blank=True, null=True)
    fecha_ing = models.CharField(max_length=36, blank=True, null=True)
    fecha_dev = models.CharField(max_length=36, blank=True, null=True)
    ejemplares = models.CharField(max_length=36, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'rep_tmp_devoluciones'


class RepTmpSaldos(models.Model):
    id_cliente = models.BigIntegerField(blank=True, null=True)
    saldo = models.BigIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'rep_tmp_saldos'


class Reversar(models.Model):
    numcomprob = models.FloatField(blank=True, null=True)
    numero = models.FloatField(blank=True, null=True)
    docto_otros = models.CharField(max_length=60, blank=True, null=True)
    fecha = models.DateField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'reversar'


class Susaltas(models.Model):
    id_suscripcion = models.BigIntegerField(blank=True, null=True)
    fecha = models.CharField(max_length=36, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'susaltas'


class Susbajas(models.Model):
    id_suscripcion = models.FloatField(blank=True, null=True)
    fecha = models.CharField(max_length=36, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'susbajas'


class Suscategoria(models.Model):
    codigo = models.CharField(primary_key=True, max_length=9)
    categoria = models.CharField(max_length=150, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'suscategoria'


class Suscorreo(models.Model):
    id_suscripcion = models.FloatField(blank=True, null=True)
    casilla = models.CharField(max_length=30, blank=True, null=True)
    correo = models.CharField(max_length=300, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'suscorreo'


class Suscripcion(models.Model):
    num_suscripcion = models.CharField(max_length=30)
    fecha_suscripcion = models.DateField(blank=True, null=True)
    fecha_inicio = models.DateField(blank=True, null=True)
    fecha_fin = models.DateField(blank=True, null=True)
    contador = models.CharField(max_length=9, blank=True, null=True)
    id_suscripcion = models.CharField(max_length=3, blank=True, null=True)
    id_agente = models.CharField(max_length=6, blank=True, null=True)
    id_documento = models.CharField(max_length=3, blank=True, null=True)
    numero_documento = models.CharField(max_length=33, blank=True, null=True)
    valorcancelar = models.CharField(max_length=15, blank=True, null=True)
    dom_factura = models.CharField(max_length=450, blank=True, null=True)
    ciu_factura = models.CharField(max_length=150, blank=True, null=True)
    com_factura = models.CharField(max_length=60, blank=True, null=True)
    estado = models.IntegerField(blank=True, null=True)
    tienecorreo = models.CharField(max_length=3, blank=True, null=True)
    tienevacaciones = models.CharField(max_length=3, blank=True, null=True)
    repartoalternativo = models.CharField(max_length=3, blank=True, null=True)
    suspendevacaciones = models.CharField(max_length=3, blank=True, null=True)
    repaltervacaciones = models.CharField(max_length=3, blank=True, null=True)
    id_actividad = models.CharField(max_length=6, blank=True, null=True)
    nota_sin_cargo = models.CharField(max_length=75, blank=True, null=True)
    categoria = models.CharField(max_length=60, blank=True, null=True)
    ejemplares = models.FloatField(blank=True, null=True)
    fecha_extendida = models.DateField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'suscripcion'


class SuscripcionProccess(models.Model):
    id_suscripcion = models.CharField(max_length=15, blank=True, null=True)
    fecha_suscripcion = models.DateField(blank=True, null=True)
    fecha_inicio = models.DateField(blank=True, null=True)
    fecha_fin = models.DateField(blank=True, null=True)
    fecha_proceso = models.DateField(blank=True, null=True)
    movimiento = models.CharField(max_length=3, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'suscripcion_proccess'


class Suscripcionantigua(models.Model):
    num_suscripcion = models.CharField(primary_key=True, max_length=30)
    fecha_suscripcion = models.DateField(blank=True, null=True)
    fecha_inicio = models.DateField(blank=True, null=True)
    fecha_fin = models.DateField(blank=True, null=True)
    contador = models.CharField(max_length=9, blank=True, null=True)
    id_suscripcion = models.CharField(max_length=3, blank=True, null=True)
    id_agente = models.CharField(max_length=6, blank=True, null=True)
    id_documento = models.CharField(max_length=3, blank=True, null=True)
    numero_documento = models.CharField(max_length=33, blank=True, null=True)
    valorcancelar = models.CharField(max_length=15, blank=True, null=True)
    dom_factura = models.CharField(max_length=450, blank=True, null=True)
    ciu_factura = models.CharField(max_length=150, blank=True, null=True)
    com_factura = models.CharField(max_length=60, blank=True, null=True)
    estado = models.CharField(max_length=3, blank=True, null=True)
    tienecorreo = models.CharField(max_length=3, blank=True, null=True)
    tienevacaciones = models.CharField(max_length=3, blank=True, null=True)
    repartoalternativo = models.CharField(max_length=3, blank=True, null=True)
    suspendevacaciones = models.CharField(max_length=3, blank=True, null=True)
    repaltervacaciones = models.CharField(max_length=3, blank=True, null=True)
    id_actividad = models.CharField(max_length=6, blank=True, null=True)
    nota_sin_cargo = models.CharField(max_length=75, blank=True, null=True)
    categoria = models.CharField(max_length=60, blank=True, null=True)
    ejemplares = models.FloatField(blank=True, null=True)
    fecha_extendida = models.DateField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'suscripcionantigua'


class Suscriptor(models.Model):
    id_suscripcion = models.FloatField()
    rut = models.FloatField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'suscriptor'


class Susrecaudador(models.Model):
    codigo = models.CharField(max_length=3, blank=True, null=True)
    recaudador = models.CharField(max_length=60, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'susrecaudador'


class Susrenovadas(models.Model):
    id_suscripcion = models.IntegerField(blank=True, null=True)
    id_anterior = models.IntegerField(blank=True, null=True)
    fecha_renovacion = models.CharField(max_length=36, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'susrenovadas'


class Susrepartidores(models.Model):
    codigo = models.BigIntegerField(primary_key=True)
    nombre = models.CharField(max_length=150, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'susrepartidores'


class Susrepartoalternativo(models.Model):
    id_suscripcion = models.FloatField(blank=True, null=True)
    dom_alterno = models.CharField(max_length=450, blank=True, null=True)
    ciu_alterno = models.CharField(max_length=90, blank=True, null=True)
    com_alterno = models.CharField(max_length=150, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'susrepartoalternativo'


class Susrepartos(models.Model):
    id_suscripcion = models.FloatField(primary_key=True)
    id_repartidor = models.FloatField(blank=True, null=True)
    nom_contact = models.CharField(max_length=450, blank=True, null=True)
    dom_reparto = models.CharField(max_length=450, blank=True, null=True)
    ciu_reparto = models.CharField(max_length=150, blank=True, null=True)
    com_reparto = models.CharField(max_length=150, blank=True, null=True)
    villa_reparto = models.CharField(max_length=450, blank=True, null=True)
    casa_reparto = models.CharField(max_length=150, blank=True, null=True)
    telefono_reparto = models.CharField(max_length=150, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'susrepartos'


class SusrepartosInf(models.Model):
    id_suscripcion = models.FloatField(blank=True, null=True)
    nombre = models.CharField(max_length=450, blank=True, null=True)
    direccion_reparto = models.CharField(max_length=450, blank=True, null=True)
    ciudad_reparto = models.CharField(max_length=120, blank=True, null=True)
    agente = models.CharField(max_length=60, blank=True, null=True)
    tipo = models.CharField(max_length=60, blank=True, null=True)
    fecha_suscrip = models.DateField(blank=True, null=True)
    fecha_inicio = models.DateField(blank=True, null=True)
    fecha_fin = models.DateField(blank=True, null=True)
    fecha_ext = models.DateField(blank=True, null=True)
    categoria = models.CharField(max_length=60, blank=True, null=True)
    repartidor = models.CharField(max_length=60, blank=True, null=True)
    movimiento = models.FloatField(blank=True, null=True)
    rut = models.CharField(max_length=60, blank=True, null=True)
    ejemplares = models.FloatField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'susrepartos_inf'


class Sustipo(models.Model):
    codigo = models.FloatField(blank=True, null=True)
    descripcion = models.CharField(max_length=75, blank=True, null=True)
    valor = models.FloatField(blank=True, null=True)
    ediciones = models.FloatField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'sustipo'


class Susvacaciones(models.Model):
    id_suscripcion = models.FloatField(blank=True, null=True)
    fecha_inicio = models.CharField(max_length=36, blank=True, null=True)
    fecha_fin = models.CharField(max_length=36, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'susvacaciones'


class Tiposnota(models.Model):
    id_tipo = models.CharField(max_length=3, blank=True, null=True)
    descripcion = models.CharField(max_length=90, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'tiposnota'


class TmpAgentesPagar(models.Model):
    id_cliente = models.FloatField(blank=True, null=True)
    fecha_dev = models.DateField(blank=True, null=True)
    ejemplares = models.FloatField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'tmp_agentes_pagar'


class TmpAvisospublicar(models.Model):
    id_aviso = models.CharField(primary_key=True, max_length=60)
    fecha = models.CharField(max_length=60, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'tmp_avisospublicar'


class TmpBoletasAvisos(models.Model):
    id_boleta = models.CharField(max_length=18, blank=True, null=True)
    total_pagar = models.CharField(max_length=60, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'tmp_boletas_avisos'


class TmpBoletasNotas(models.Model):
    id_boleta = models.CharField(max_length=18, blank=True, null=True)
    total_pagar = models.CharField(max_length=60, blank=True, null=True)
    id_nota = models.CharField(max_length=18, blank=True, null=True)
    id_user = models.CharField(max_length=6, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'tmp_boletas_notas'


class TmpBoletasSuscripciones(models.Model):
    id_boleta = models.CharField(max_length=27, blank=True, null=True)
    total_pagar = models.CharField(max_length=33, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'tmp_boletas_suscripciones'


class TmpComprobante(models.Model):
    nro_copia = models.BigIntegerField()
    id_comprobante = models.BigIntegerField(blank=True, null=True)
    nombre = models.CharField(max_length=750, blank=True, null=True)
    rut = models.CharField(max_length=60, blank=True, null=True)
    nro_documento = models.FloatField(blank=True, null=True)
    fecha_emision = models.DateField(blank=True, null=True)
    emision = models.DateField(blank=True, null=True)
    fecha_vence = models.DateField(blank=True, null=True)
    valor_documento = models.BigIntegerField(blank=True, null=True)
    monto_pagado = models.BigIntegerField(blank=True, null=True)
    saldo = models.BigIntegerField(blank=True, null=True)
    usuario = models.CharField(max_length=150, blank=True, null=True)
    pago = models.BigIntegerField(blank=True, null=True)
    banco = models.CharField(max_length=180, blank=True, null=True)
    serie = models.CharField(max_length=60, blank=True, null=True)
    cheque = models.CharField(max_length=90, blank=True, null=True)
    fecha_cobro = models.DateField(blank=True, null=True)
    valor = models.BigIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'tmp_comprobante'


class TmpEntregasvsdev(models.Model):
    id_cliente = models.IntegerField(blank=True, null=True)
    nombre = models.CharField(max_length=300, blank=True, null=True)
    direccion = models.CharField(max_length=300, blank=True, null=True)
    rut = models.BigIntegerField(blank=True, null=True)
    digito = models.CharField(max_length=3, blank=True, null=True)
    fecha = models.DateField(blank=True, null=True)
    ent = models.IntegerField(blank=True, null=True)
    devolucion = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'tmp_entregasvsdev'


class TmpFactPend(models.Model):
    rut = models.BigIntegerField(blank=True, null=True)
    digito = models.CharField(max_length=3, blank=True, null=True)
    nombre = models.CharField(max_length=300, blank=True, null=True)
    numero = models.IntegerField(blank=True, null=True)
    fecha_emision = models.DateField(blank=True, null=True)
    agente = models.CharField(max_length=300, blank=True, null=True)
    total = models.IntegerField(blank=True, null=True)
    abono = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'tmp_fact_pend'


class TmpFacturasNotas(models.Model):
    id_factura = models.CharField(max_length=30, blank=True, null=True)
    total_pagar = models.CharField(max_length=33, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'tmp_facturas_notas'


class TmpFacturasUser(models.Model):
    numero = models.BigIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'tmp_facturas_user'


class TmpFacturaspagasagente(models.Model):
    id_factura = models.CharField(max_length=18, blank=True, null=True)
    rut = models.CharField(max_length=36, blank=True, null=True)
    digito = models.CharField(max_length=3, blank=True, null=True)
    nombre = models.CharField(max_length=300, blank=True, null=True)
    fecha_emision = models.CharField(max_length=36, blank=True, null=True)
    fecha_pago = models.CharField(max_length=36, blank=True, null=True)
    pago = models.CharField(max_length=33, blank=True, null=True)
    agente = models.CharField(max_length=600, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'tmp_facturaspagasagente'


class TmpFacturaspagasrecaudador(models.Model):
    id_factura = models.CharField(max_length=18, blank=True, null=True)
    rut = models.CharField(max_length=33, blank=True, null=True)
    digito = models.CharField(max_length=3, blank=True, null=True)
    nombre = models.CharField(max_length=450, blank=True, null=True)
    fecha_emision = models.CharField(max_length=36, blank=True, null=True)
    fecha_pago = models.CharField(max_length=36, blank=True, null=True)
    pago = models.CharField(max_length=33, blank=True, null=True)
    recaudador = models.CharField(max_length=90, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'tmp_facturaspagasrecaudador'


class TmpNotasAvisos(models.Model):
    id_notaventa = models.CharField(max_length=18, blank=True, null=True)
    id_aviso = models.CharField(max_length=18, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'tmp_notas_avisos'


class TmpNotasPendientes(models.Model):
    numero = models.BigIntegerField(blank=True, null=True)
    id_documento = models.CharField(max_length=60, blank=True, null=True)
    fecha = models.DateField(blank=True, null=True)
    rut_cliente = models.CharField(max_length=60, blank=True, null=True)
    total_pagar = models.BigIntegerField(blank=True, null=True)
    canje = models.CharField(max_length=60, blank=True, null=True)
    fechapublicacion = models.CharField(max_length=300, blank=True, null=True)
    agente = models.CharField(max_length=60, blank=True, null=True)
    tipo = models.CharField(max_length=60, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'tmp_notas_pendientes'


class TmpNotasSuscripciones(models.Model):
    id_notaventa = models.CharField(max_length=18, blank=True, null=True)
    id_suscripcion = models.CharField(max_length=15, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'tmp_notas_suscripciones'


class TmpRepartoDev(models.Model):
    id_cliente = models.IntegerField(primary_key=True)
    devoluciones = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'tmp_reparto_dev'


class TmpRepartoEnt(models.Model):
    id_cliente = models.FloatField(primary_key=True)
    rut = models.FloatField(blank=True, null=True)
    digito = models.CharField(max_length=3, blank=True, null=True)
    nombre = models.CharField(max_length=60, blank=True, null=True)
    entregas = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'tmp_reparto_ent'


class Users(models.Model):
    codigo = models.BigIntegerField(blank=True, null=True)
    nombre = models.CharField(max_length=150, blank=True, null=True)
    userlogin = models.CharField(max_length=75, blank=True, null=True)
    userkey = models.CharField(max_length=4875, blank=True, null=True)
    id_perfil = models.BigIntegerField(blank=True, null=True)
    id_nivel = models.BigIntegerField(blank=True, null=True)
    rut = models.BigIntegerField(blank=True, null=True)
    digito = models.CharField(max_length=3, blank=True, null=True)
    estado = models.BigIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'users'


class Usersprofile(models.Model):
    perfil = models.CharField(max_length=3, blank=True, null=True)
    descripcion = models.CharField(max_length=39, blank=True, null=True)
    archivos_accesos = models.CharField(max_length=9, blank=True, null=True)
    avisos_accesos = models.CharField(max_length=9, blank=True, null=True)
    suscrip_accesos = models.CharField(max_length=9, blank=True, null=True)
    notas_accesos = models.CharField(max_length=3, blank=True, null=True)
    caja_accesos = models.CharField(max_length=12, blank=True, null=True)
    reparto_accesos = models.CharField(max_length=12, blank=True, null=True)
    tools_accesos = models.CharField(max_length=15, blank=True, null=True)
    nivel_anular = models.CharField(max_length=3, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'usersprofile'


class ViewClientesBloqueo(models.Model):
    rut = models.CharField(max_length=60, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'view_clientes_bloqueo'


class Weditor(models.Model):
    nro_pagina = models.BigIntegerField(blank=True, null=True)
    tamanocm = models.CharField(max_length=60, blank=True, null=True)
    columnas = models.CharField(max_length=60, blank=True, null=True)
    fecha = models.DateField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'weditor'
