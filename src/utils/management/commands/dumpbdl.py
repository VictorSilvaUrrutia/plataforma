from django.db import transaction
from django.core.management.base import BaseCommand

from lugares.migrate import run_regiones_comunas as regiones_y_comunas


class Command(BaseCommand):
    help = 'Hace dump de los datos de bdl1.0 a bdl2.0'

    def handle(self, *args, **options):
        try:
            with transaction.atomic():
                regiones_y_comunas()
        except Exception as exception:
            msg = "Error en el dump!!!\n %s" % (str(exception),)
            self.stdout.write(self.style.ERROR(msg))
        else:
            self.stdout.write(self.style.SUCCESS('Dump terminado!'))
