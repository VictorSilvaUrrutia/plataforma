from django import forms


class UtilForm(forms.ModelForm):

    autofocus_field = None

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        if self.autofocus_field:
            self.fields[self.autofocus_field].widget.attrs.update({'autofocus': ''})
