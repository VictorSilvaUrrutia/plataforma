from django.test import TestCase
from django.contrib.auth.models import User


def create_superuser(username, email, password):
    return User.objects.create_superuser(username=username, email=email, password=password)


class TestResponse(TestCase):

    def check_responses(self, client, routes=[], status_code=200):
        for route in routes:
            response = client.get(route)
            self.assertEqual(response.status_code, status_code)

    def assert_responses(self, client, routes=[], status_code=200):
        for (route, template) in routes:
            response = client.get(route)
            self.assertTemplateUsed(response, template)
            self.assertEqual(response.status_code, status_code)
