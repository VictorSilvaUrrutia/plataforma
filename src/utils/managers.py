from django.db.models import Q


class FindMixin(object):
    targets = []

    def find(self, search):
        if not search:
            return self

        base_qs = Q()

        for target in self.targets:
            base_qs |= Q(**{target:search})

        return self.filter(base_qs)
