from django.db import transaction
from django.contrib import messages
from django.urls import reverse_lazy
from django.contrib.messages.views import SuccessMessageMixin
from django.views.generic import DeleteView, CreateView, UpdateView, TemplateView, DetailView
from django_tables2 import SingleTableView

from .mixins import PermissionsRequiredMixin
from .models import get_singular_name


class PermissionView(PermissionsRequiredMixin, TemplateView):
    required_permissions = ()


class UtilDetailView(PermissionsRequiredMixin, DetailView):
    required_permissions = ()


class UtilTableView(SingleTableView):
    change_permission = ''
    delete_permission = ''
    paginate_by = 20
    order_by = 'id'

    def get_table(self, **kwargs):
        table = super().get_table(**kwargs)

        if self.request.user.has_perm(self.change_permission):
            table.can_edit = True

        if self.request.user.has_perm(self.delete_permission):
            table.can_delete = True

        return table

    def get_queryset(self):
        search = self.request.GET.get('q', False)
        if search:
            return self.model_class.objects.find(search).order_by('id')
        return self.model_class.objects.get_queryset()

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['q'] = self.request.GET.get('q', '')
        return context


class UtilListView(PermissionsRequiredMixin, UtilTableView):
    required_permissions = ()


class UtilCreateView(SuccessMessageMixin, PermissionsRequiredMixin, CreateView):
    success_message = 'Modelo creado exitosamente.'
    required_permissions = ()


class UtilUpdateView(SuccessMessageMixin, PermissionsRequiredMixin, UpdateView):
    success_message = 'Modelo actualizado exitosamente.'
    required_permissions = ()


class UtilDeleteView(PermissionsRequiredMixin, DeleteView):
    success_message = 'Modelo eliminado exitosamente'
    required_permissions = ()

    def delete(self, request, *args, **kwargs):
        messages.success(self.request, self.success_message)
        return super().delete(request, *args, **kwargs)


def get_success_route(model):
    app_name = model._meta.app_label
    model_name = str(model.__name__).lower()
    return reverse_lazy('{0}:{1}_list'.format(app_name, model_name))


class UtilCreateView2(SuccessMessageMixin, PermissionsRequiredMixin, CreateView):
    template_name = 'views/form.html'
    required_permissions = ()

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['form_title'] = self.form_title
        context['success_url'] = self.success_url
        return context

    @property
    def success_message(self):
        if not self.model:
            raise NotImplementedError('Define a model')
        model_name = get_singular_name(self.model)
        return '{} creado exitosamente'.format(model_name)

    @property
    def form_title(self):
        if not self.model:
            raise NotImplementedError('Define a model')
        model_name = get_singular_name(self.model)
        return 'Crear {}'.format(model_name)

    @property
    def success_url(self):
        return get_success_route(self.model)


class UtilUpdateView2(SuccessMessageMixin, PermissionsRequiredMixin, UpdateView):
    template_name = 'views/form.html'
    required_permissions = ()

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['form_title'] = self.form_title
        context['success_url'] = self.success_url
        return context

    @property
    def form_title(self):
        model_name = get_singular_name(self.model)
        return 'Actualizar {}'.format(model_name)

    @property
    def success_message(self):
        model_name = get_singular_name(self.model)
        return '{} actualizado exitosamente.'.format(model_name)

    @property
    def success_url(self):
        return get_success_route(self.model)


class UtilDeleteView2(PermissionsRequiredMixin, DeleteView):
    template_name = 'views/confirm_delete.html'
    required_permissions = ()

    def delete(self, request, *args, **kwargs):
        messages.success(self.request, self.success_message)
        return super().delete(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['delete_title'] = self.delete_title
        context['success_url'] = self.success_url
        return context

    @property
    def delete_title(self):
        model_name = get_singular_name(self.model)
        return 'Eliminar {} '.format(model_name)

    @property
    def success_message(self):
        model_name = get_singular_name(self.model)
        return '{} eliminado exitosamente'.format(model_name)

    @property
    def success_url(self):
        return get_success_route(self.model)


class BaseFormsetView(object):
    prefix = 'formset'
    formset_class = None
    formset_name = 'formset'

    def get_context_data(self, **kwargs):
        data = super().get_context_data(**kwargs)
        data[self.formset_name] = self.get_formset()
        return data


class CreateFormsetView(BaseFormsetView, UtilCreateView2):

    def get_formset(self):
        if self.request.POST:
            if not self.formset_class:
                raise NotImplementedError('Agregar formset_class')
            return self.formset_class(self.request.POST, prefix=self.prefix)
        return self.formset_class(prefix=self.prefix)

    def form_valid(self, form):
        data = self.get_context_data()
        formset = data[self.formset_name]
        with transaction.atomic():
            if formset.is_valid():
                self.object = form.save()
                formset.instance = self.object
                formset.save()
                return super().form_valid(form)
        return self.render_to_response(self.get_context_data(form=form))


class UpdateFormsetView(BaseFormsetView, UtilUpdateView2):

    def get_formset(self):
        if self.request.POST:
            if not self.formset_class:
                raise NotImplementedError('Agregar formset_class')
            return self.formset_class(self.request.POST, instance=self.object, prefix=self.prefix)
        return self.formset_class(instance=self.object, prefix=self.prefix)

    def form_valid(self, form):
        data = self.get_context_data()
        formset = data[self.formset_name]
        with transaction.atomic():
            if formset.is_valid():
                self.object = form.save()
                formset.instance = self.object
                formset.save(commit=False)
                for obj in formset.deleted_objects:
                    obj.delete()
                formset.save()
                return super().form_valid(form)
        return self.render_to_response(self.get_context_data(form=form))
