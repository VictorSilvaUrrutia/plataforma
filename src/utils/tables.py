from django.utils.safestring import mark_safe

import django_tables2 as tables


class ResponsiveTable(object):
    '''Mixin para hacer las tablas responsivas.'''

    class Meta:
        template = 'django_tables2/bootstrap-responsive.html'
        attrs = {'class': 'table table-bordered table-hover dataTable'}


class UtilTable(tables.Table):
    options = tables.Column(verbose_name='Opciones',
                            orderable=False,
                            empty_values=(),
                            attrs={
                                'td': {'class': 'text-center'},
                                'th': {"class": "text-center col-md-2"}})
    can_edit = False
    can_delete = False
    edit_text = 'Editar'
    edit_icon = 'fa fa-pencil-square-o'
    delete_text = 'Eliminar'
    delete_icon = 'fa fa-user-times'

    def reverse_update(self, record):
        raise NotImplementedError('Debes implementar el metodo reverse_update.')

    def reverse_delete(self, record):
        raise NotImplementedError('Debes implementar el metodo reverse_delete.')

    def edit_option(self, url):
        if not self.can_edit:
            return ''

        return '''
            <a href="{0}" class="btn btn-sm btn-success tooltip-link"
                data-original-title="{1}">
                <i class="{2}"></i> {1}
            </a>
        '''.format(url, self.edit_text, self.edit_icon)

    def delete_option(self, url):
        if not self.can_delete:
            return ''

        return '''
        <a href="{0}" class="btn btn-sm btn-danger tooltip-link"
            data-original-title="{1}">
            <i class="{2}"></i> {1}
        </a>
        '''.format(url, self.delete_text, self.delete_icon)

    def render_options(self, value, record):
        return mark_safe(
            self.edit_option(self.reverse_update(record)) +
            self.delete_option(self.reverse_delete(record))
        )


def get_dropdown(urls, title='Opciones', classnames='btn btn-success'):
    url_list = ''.join(['<li>' + url + '</li>' for url in urls])
    template = '''
        <div class="dropup">
            <button class="%s dropdown-toggle" type="button" data-toggle="dropdown">%s
            <span class="caret"></span></button>
            <ul class="dropdown-menu">%s</ul>
        </div>
    ''' % (classnames, title, url_list)
    return mark_safe(template)


def get_href(url, onclick='', title=''):
    return '<a href="%s" onclick="%s">%s</a>' % (url, onclick, title)
