from django.apps import AppConfig


class InfoFacturasConfig(AppConfig):
    name = 'info_facturas'
