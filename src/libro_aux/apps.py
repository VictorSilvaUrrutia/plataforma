from django.apps import AppConfig


class LibroAuxConfig(AppConfig):
    name = 'libro_aux'
