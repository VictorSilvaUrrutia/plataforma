from django.contrib import admin
from rangefilter.filter import DateRangeFilter, DateTimeRangeFilter
from .models import Notasventa
from .forms import RegForm, RegModelForm


class AdminFecha_multiple(admin.ModelAdmin):
    list_display = ["codigo", "fecha_multiple",]


class AdminNotasventa(admin.ModelAdmin):
    form = RegModelForm
    list_display = ["numero",   "num_orden" ,"publicidad", "publicacion","id_agente" ]
    fields = ['rut_cliente', 'id_agente', ('publicacion', 'publicidad'),'fechapublicacion','fechapublicacion_fin','cod_avtarifa',('color','nro_pagina'),'seccion', ('tamanocm','columnas'),'id_documento',]
    ordering = ('-numero',)

    search_fields = ["numero"]
    raw_id_fields = ('rut_cliente',)



    list_filter = (
         ('fecha', DateRangeFilter),
    )

#Registro del modelo y el administrador
admin.site.register(Notasventa, AdminNotasventa,)
#admin.site.register(Fecha_multiple, AdminFecha_multiple,)




