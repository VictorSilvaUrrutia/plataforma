from django.urls import reverse_lazy
from utils.tables import UtilTable
from .models import Notasventa

class NotasVentaTable(UtilTable):
    class Meta:
        model = Notasventa
        template = 'django_tables2/bootstrap-responsive.html'
        sequence = (
            'numero'
            'num_orden',
            'num_documento',
            #'rut_cliente',
            #'id_agente',
            #'fecha',
            #'publicacion',
            #'publicidad',
            #'fechapublicacion',
            #'fechapublicacion_fin',
            #'cod_avtarifa',
            #'color',
            #'nro_pagina',
            #'seccion',
            #'tamanocm',
            #'columnas',
            #'id_documento',
            #'id_agencia',
        )

    def reverse_update(self, record):
        return reverse_lazy('Notasventa:Notasventa_update', kwargs={'pk': record.pk})

    def reverse_delete(self, record):
        return reverse_lazy('Notasventa:Notasventa_delete', kwargs={'pk': record.pk})