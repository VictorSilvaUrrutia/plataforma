from datetime import datetime
from Archivo.models import Avtarifa
from Archivo.models import Avagentes
from Archivo.models import Clientes,Documentos
from Publicaciones.models import Avpublicaciongeneral
from django.core.exceptions import ValidationError
from django.db import models
from django.utils import timezone
from model_utils import Choices




class Avsecciondiario(models.Model):
    cod_seccion = models.IntegerField(primary_key=True)
    nombre = models.CharField(max_length=600, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'avsecciondiario'

    def __str__(self):
        return '{0} ({1})'.format(self.cod_seccion, self.nombre)



def validate_date(date):
    "Verifica que la fecha a guardar no sea anterior a la fecha actual"
    if date < timezone.now().date():
        raise ValidationError("Fecha anterior al día de hoy no es valido")



class Notasventa(models.Model):

#Alto de la página

    Sin_pag = 0
    uno = 1
    dos = 2
    tres = 3
    cuatro = 4
    cinco=  5
    seis = 6
    siete = 7
    ocho = 8
    nueve = 9
    diez = 10
    once = 11
    doce = 12
    trece = 13
    catorce = 14
    quince = 15
    diesiseis = 16
    diesisiete = 17
    diesiocho = 18
    diesinueve = 19
    veinte = 20
    veintiuno = 21
    veintidos = 22
    veintitres = 23
    veinticuatro = 24
    veinticinco = 25
    veintiseis = 26
    veintisiente = 27
    veintiocho = 28
    veintinueve = 29
    trienta = 30
    treintayuno = 31
    treintaydos = 32
    treintaytres = 33
    treintaycuatro = 34
    trintaycinco = 35
    treintayseis = 36
    treintaysiete = 37


#Ancho de la página
    STATUS_CHOICES = (
        (Sin_pag, 0),(uno, 1),(dos, 2), (tres, 3), (cuatro, 4), (cinco, 5),(seis, 6),(siete, 7),(ocho, 8),
    )

#Alto de la página
    STATUS_CHOICES2 = (
        (uno, 1), (dos, 2), (tres, 3), (cuatro, 4), (cinco, 5), (seis, 6), (siete, 7), (ocho, 8),(nueve, 9),(diez, 10),(once, 11),(doce, 12),(trece, 13),(catorce, 14),(quince, 15),(diesiseis, 16),(diesisiete, 17),(diesiocho, 18),(diesinueve, 19),(veinte, 20),(veintiuno, 21),(veintidos, 22),(veintitres, 23),(veinticuatro, 24),(veinticinco, 25),(veintiseis, 26),
        (veintisiente, 27),(veintiocho, 28),(veintinueve, 29),(trienta, 30),(treintayuno, 31),(treintaydos, 32),(treintaytres, 33),(treintaycuatro, 34),(trintaycinco, 35),(treintayseis, 36),(treintaysiete, 37),
    )

#Numero de página

    STATUS_CHOICES3 = (
        (uno, 1), (dos, 2), (tres, 3), (cuatro, 4), (cinco, 5), (seis, 6), (siete, 7),
    )


    def save(self,):
        "Toma el valor maximo de la columna de la base de datos y lo autoincrementa antes de guardar"
        top = Notasventa.objects.order_by('-numero','-num_documento','-num_orden')[0]
        self.numero = top.numero + 1
        self.num_documento = top.num_documento + 1
        self.num_orden = top.num_orden + 1
        super(Notasventa, self).save()

    opciones = Choices('Publicidad', 'Canje','Bonificación')
    colores = Choices('Blanco y Negro', 'Color')

    FECHASS = (
        (True, 'Rango de Fechas'),
        (False, 'Selección Multiple'),
    )
#Campos de la base de datos

    numero = models.IntegerField(primary_key=True,editable=False,verbose_name='Orden de publicación')
    num_orden = models.IntegerField(editable=False)
    num_documento = models.IntegerField(editable=False)
    rut_cliente =models.ForeignKey(Clientes,db_column='RUT_CLIENTE',verbose_name="Cliente",help_text='Seleccionar lupa para buscar cliente, Si desea ingresar el RUT de un cliente ya registrado escribalo sin puntos y sin digito verificador',on_delete=models.PROTECT)
    id_agente = models.ForeignKey(Avagentes,db_column='ID_AGENTE', verbose_name="Agente",default=51,on_delete=models.PROTECT)
    fecha = models.DateField(("Fecha de Ingreso"), default=datetime.now(), editable=False)
    publicacion = models.ForeignKey(Avpublicaciongeneral,db_column='PUBLICACION', verbose_name="Publicar en",on_delete=models.PROTECT)
    publicidad = models.CharField(max_length=60,choices=opciones,default=opciones.Publicidad,verbose_name="Tipo")
    fechapublicacion = models.DateField(verbose_name="Fecha de inicio de publicación", default=None,validators=[validate_date], null=True)
    fechapublicacion_fin = models.DateField(verbose_name="Fecha de fin de publicación", default=None,validators=[validate_date],db_column='FIN_FECHAPUBLICACION',null=True )
    #fecha_multiple = models.ManyToManyField(Fecha_multiple,db_column='fecha_multiple')
    cod_avtarifa = models.ForeignKey(Avtarifa,db_column='COD_AVTARIFA',verbose_name="Tipo de aviso",on_delete=models.PROTECT)
    color = models.IntegerField(max_length=60,choices=colores)
    nro_pagina = models.IntegerField(choices=STATUS_CHOICES,verbose_name='Página')
    seccion = models.ForeignKey(Avsecciondiario,db_column='SECCION',help_text='Dimensiones',on_delete=models.PROTECT)
    tamanocm = models.IntegerField(choices=STATUS_CHOICES2,verbose_name='Alto')
    columnas = models.IntegerField(choices=STATUS_CHOICES,verbose_name='Ancho')
    id_documento = models.ForeignKey(Documentos,db_column='ID_DOCUMENTO',verbose_name='Documento',on_delete=models.PROTECT)
    id_agencia = models.CharField(max_length=200,verbose_name='Agencia')
    #coduser = models.IntegerField(validators=[your_view])#######No Utilizar
    #orden_agencia = models.CharField(max_length=600, blank=True, null=True)
    #valor = models.BigIntegerField(blank=True, null=True)
    #anulada = models.FloatField(blank=True, null=True)
    #cancelada = models.CharField(max_length=60, blank=True, null=True)
    #facturada = models.FloatField(blank=True, null=True)
    #descuento = models.CharField(max_length=60, blank=True, null=True)
    #total_pagar = models.CharField(max_length=60, blank=True, null=True)
    #das = models.CharField(max_length=60, blank=True, null=True)
    #subseccion = models.CharField(max_length=60, blank=True, null=True)
    #direccion_factura = models.CharField(max_length=450, blank=True, null=True)
    #email_factura = models.CharField(max_length=450, blank=True, null=True)
    #telefono_factura = models.BigIntegerField(blank=True, null=True)
    #es_canje = models.IntegerField(blank=True, null=True)
    #ciudad = models.IntegerField(blank=True, null=True)
    # hes_das = models.BigIntegerField(blank=True, null=True)

    class Meta:
        verbose_name = 'Orden de publicación'
        verbose_name_plural = 'Orden de publicación'
        managed = False
        db_table = 'notasventa'
        ordering = ["numero"]




