from django.db.models import Manager, Q

class NotasVentaManager(Manager):
    def find(self, search):
        if not search:
            return self

        qs = Q(numero__icontains=search)
        return self.filter(qs)