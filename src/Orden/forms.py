from django import forms
from .models import Notasventa,Avsecciondiario
from Archivo.models import Clientes,Documentos,Avagentes,Avtarifa
from Publicaciones.models import Avpublicaciongeneral






class RegForm(forms.Form): # RegForm es el nombre para el registro del formulario


    numero = forms.IntegerField(required=False,disabled=True)
    num_orden= forms.IntegerField(required=False,disabled=True)
    num_documento= forms.IntegerField(required=False,disabled=True)
    #rut_cliente= forms.ModelChoiceField(queryset=Clientes.objects.all())
    #id_agente= forms.ModelChoiceField(queryset=Avagentes.objects.all())
    #fecha = forms.DateField(label='Seleccione fecha', widget=forms.SelectDateWidget(years=range(1910, 2099)))
    #publicacion= forms.ModelChoiceField(queryset=Avpublicaciongeneral.objects.all())
    #publicidad= forms.CharField()
    #fechapublicacion = forms.DateField(label='Seleccione fecha', widget=forms.SelectDateWidget(years=range(1910, 2099)))
    #fechapublicacion_fin= forms.DateField(label='Seleccione fecha', widget=forms.SelectDateWidget(years=range(1910, 2099)))
    #cod_avtarifa=forms.ModelChoiceField(queryset=Avtarifa.objects.all())
    #color= forms.CharField()
    #nro_pagina=forms.CharField()
    #seccion= forms.ModelChoiceField(queryset=Avsecciondiario.objects.all())
    #tamanocm= forms.IntegerField()
    #columnas= forms.IntegerField()
    #id_documento= forms.ModelChoiceField(queryset=Documentos.objects.all())
    #id_agencia= forms.IntegerField()


class RegModelForm(forms.ModelForm):


    class Meta:
        modelo = Notasventa
        campos = ["numero","num_orden","num_documento",]




