
from django.shortcuts import render
from .forms import RegForm
from .models import Notasventa
from django.urls import reverse_lazy
from utils.views import UtilListView, UtilDeleteView, UtilCreateView, UtilUpdateView
from . import forms, models, tables
from django_filters.views import FilterView


class ListView(FilterView, UtilListView):
    template_name = 'Notasventa/list.html'
    table_class = tables.NotasVentaTable
    paginate_by = 20
    model_class = models.Notasventa
    required_permissions = ('Notasventa.view',)
    change_permission = 'Notasventa.change_Notasventa'
    delete_permission = 'NotasVenta.delete_Notasventa'



# Create your views here.
def inicio(request): # Función para el template HTML con formulario llamado "el_formulario"
    form = RegForm(request.POST or None) #Agregamos el método POST --> request.POST
    #print (dir(form)) # Permite mostrar en consola los métodos que podemos invocar con 'form'
    if form.is_valid():
        form_data = form.cleaned_data
        numero2 = form_data.get("numero")
        num_orden2 = form_data.get("num_orden")
        num_documento2 = form_data.get("num_documento")
        #rut_cliente2 = form_data.get("rut_cliente")
        #id_agente2 = form_data.get("id_agente")
        #fecha2 = form_data.get("fecha")
        #publicacion2 = form_data.get("publicacion")
        #publicidad2 = form_data.get("publicidad")
        #fechapublicacion2 = form_data.get("fechapublicacion")
        #fechapublicacion_fin2 = form_data.get("fechapublicacion_fin")
        #cod_avtarifa2 = form_data.get("cod_avtarifa")
        #color2 = form_data.get("color")
        #nro_pagina2 = form_data.get("nro_pagina")
        #seccion2 = form_data.get("seccion")
        #tamanocm2 = form_data.get("tamanocm")
        #columnas2 = form_data.get("columnas")
        #id_documento2 = form_data.get("id_documento")
        #id_agencia2 = form_data.get("id_agencia")


        objeto = Notasventa.objects.create(numero=numero2,num_orden=num_orden2,num_documento=num_documento2)

        # OTRA FORMA DE GUARDAR LOS OBJETOS:
        #objeto = Registrado()
        #objeto.nombre = nombre2
        #objeto.email = email2
        #objeto.save()



    contexto = {
        "el_formulario": form,
    }
    return render(request, "inicio.html", contexto) # Cambiamos {} por el nombre des diccionario creado 'contexto'

