cx-Oracle==7.0.0
Django==1.10.7
django-admin-rangefilter==0.4.0
django-adminlte2==0.4.1
django-filter==2.1.0
django-model-utils==3.1.2
pytz==2019.1
sqlparse==0.3.0
